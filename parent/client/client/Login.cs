﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) //sign up
        {
            this.Hide();
            Register form2 = new Register();

            form2.ShowDialog();

            this.Show();
        }

        private void button2_Click(object sender, EventArgs e)  //sign in
        {
            string username = this.usernameTB.Text;
            string password = this.passTB.Text;
            if (username.CompareTo("") == 0 || password.CompareTo("") == 0)
            {
                errortxt.Text = "Please fill all fields";
                return;
            }
            string input = Program.handleSignin(username, password);

            /* send to other computer
            Program.msg = ("200" + Helper.getPaddedNumber(username.Length, 2) + username + Helper.getPaddedNumber(password.Length, 2) + password);
            Program.waitHandle.Set();
            Program.ewh.WaitOne();
            Program.ewh.Reset();
            string input = Program.input;
            */
            if (input.Equals("1020"))
            {
                errortxt.Text = "";
                this.Hide();
                Functions form3 = new Functions(username, password);
                form3.ShowDialog();
                this.Show();

            }
            else if (input.Equals("1021"))
            {
                errortxt.Text = "Wrong Details!";
            }
           /* else
            {
                errortxt.Text = "User is already connected!";
            }*/
        }

        private void usernameTB_TextChanged(object sender, EventArgs e)
        {
            panel1.BackColor = Color.FromArgb(74, 184, 206);
            usernameTB.ForeColor = Color.FromArgb(74, 184, 206);

            panel2.BackColor = Color.WhiteSmoke;
            passTB.ForeColor = Color.WhiteSmoke;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void passTB_TextChanged(object sender, EventArgs e)
        {
            passTB.PasswordChar = '•';
            panel2.BackColor = Color.FromArgb(74, 184, 206);
            passTB.ForeColor = Color.FromArgb(74, 184, 206);

            panel1.BackColor = Color.WhiteSmoke;
            usernameTB.ForeColor = Color.WhiteSmoke;
        }
    }
}
