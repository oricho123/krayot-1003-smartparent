﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace client
{
    class DataBase
    {
        // Defining Constant Variables for DB
        public const string SERVER = "db4free.net";
        public const string USER = "smartparentdb"; // user name by which you want to access the database ( database user name- not server)
        public const string PASSWORD = "orilaelguylevy";
        public const string DATABASE = "smartparentdb";// the database name in the database server
        public const int DBPORT = 3306;

        public static MySqlConnection conn;
        public static MySqlCommand cmd;

        public DataBase()
        {
            string connectionString = "SERVER=" + SERVER + ";" + "DATABASE=" + DATABASE + ";" + "UID=" + USER + ";" + "PASSWORD=" + PASSWORD + ";" + "oldguids = true";
            conn = new MySqlConnection(connectionString);
            try
            {
                conn.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Can't connect to DataBase", MessageBoxButtons.OK, MessageBoxIcon.Error);
                conn = null;
                //error
                Environment.Exit(1);
                return;
            }

            cmd = conn.CreateCommand();
            cmd.CommandText = "CREATE TABLE t_users(username varchar(255) not null, password text not null, email text not null,   PRIMARY KEY (username))"; 
            try
            {
                int i = cmd.ExecuteNonQuery();
            }
            catch (Exception){ }  //if the table already exists
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "CREATE TABLE t_sites(site varchar(255) not null, username_users varchar(255) not null, FOREIGN KEY(username_users) REFERENCES t_users(username))";
            try
            {
                int i = cmd.ExecuteNonQuery();
            }
            catch (Exception e) { }  //if the table already exists
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "CREATE TABLE t_apps(app varchar(255) not null, username_users varchar(255) not null, FOREIGN KEY(username_users) REFERENCES t_users(username))";
            try
            {
                int i = cmd.ExecuteNonQuery();
            }
            catch (Exception e) { }  //if the table already exists
            /*MySqlDataAdapter adp = new MySqlDataAdapter(cmd, conn);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            */
        }

        ~DataBase()
        {
            if(conn!=null)
                conn.Close();
        }

        public bool isUserExists(string username)
        {
            string DbUsername;
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "select username from t_users where username = \"" + username + "\""; //check if getting an answer
            MySqlDataReader reader = cmd.ExecuteReader();//run the command
           
            if (reader.Read())
            {
                DbUsername = reader.GetValue(0).ToString();
                reader.Close();
                return (username.Equals(DbUsername));  //check if username equals to result // if the cide got to this line, it will always return true  
            }
            reader.Close();
            return false;
        }

        public bool isUserAndPassMatch(string username, string password)
        {
            string DbPassword;
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "select password from t_users where username =  \"" + username + "\"";
            MySqlDataReader reader = cmd.ExecuteReader();
           
            if (reader.Read())
            {
                DbPassword = reader.GetValue(0).ToString();
                reader.Close();
                return (password.Equals(DbPassword));  //check if passwords match  
            }
            reader.Close();
            return false;
        }


        public void addNewUser(string username, string password, string email)
        {
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "insert into t_users values(\"" + username + "\",\"" + password + "\",\"" + email + "\")";
            int i = cmd.ExecuteNonQuery();
          
        }

        public MySqlDataAdapter getBlockedSites(string username)
        {
            string query = "select site from t_sites where username_users = \"" + username + "\"";
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(query, conn);
            return dataAdapter;
        }

        public MySqlDataAdapter getBlockedApps(string username)
        {
            string query = "select app from t_apps where username_users = \"" + username + "\"";
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(query, conn);
            return dataAdapter;
        }

        public void addBlockedSite(string username, string sitename)
        {
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "insert into t_sites values(\"" + sitename + "\",\"" + username + "\")";
            int i = cmd.ExecuteNonQuery();

        }

        public void addBlockedApp(string username, string appname)
        {
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "insert into t_apps values(\"" + appname + "\",\"" + username + "\")";
            int i = cmd.ExecuteNonQuery();

        }

        public void deleteBlockedSite(string username, string sitename)
        {
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "DELETE FROM t_sites WHERE username_users = \"" + username + "\" and site = \"" + sitename + "\"";
            int i = cmd.ExecuteNonQuery();
        }

        public void deleteBlockedApp(string username, string appname)
        {
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "DELETE FROM t_apps WHERE username_users = \"" + username + "\" and app = \"" + appname + "\"";
            int i = cmd.ExecuteNonQuery();
        }

        public bool isSiteBlocked(string username, string sitename)
        {
            string DbSite;
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "select site FROM t_sites WHERE username_users = \"" + username + "\" and site = \"" + sitename + "\"";
            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                DbSite = reader.GetValue(0).ToString();
                reader.Close();
                return (sitename.Equals(DbSite));  //check if names match  
            }
            reader.Close();
            return false;
        }

        public bool isAppBlocked(string username, string appname)
        {
            string DbApp;
            cmd.Dispose();
            cmd = conn.CreateCommand();
            cmd.CommandText = "select app FROM t_apps WHERE username_users = \"" + username + "\" and app = \"" + appname + "\"";
            MySqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                DbApp = reader.GetValue(0).ToString();
                reader.Close();
                return (appname.Equals(DbApp));  //check if names match  
            }
            reader.Close();
            return false;
        }

    }
}
