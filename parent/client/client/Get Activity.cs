﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Get_Activity : Form
    {
        private const int PACK_SIZE = 4096; //udp pack size;
        private const int BUF_LEN = 65540; //max udp pack size;

        byte[] key;
        public Get_Activity()
        {
            InitializeComponent();
            key = Encoding.Default.GetBytes("orilaelguylevy12");
        }

        private void backbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Get_Activity_Load(object sender, EventArgs e)
        {
            backbtn.Enabled = false;
            string text = getText();
            if (text != null)
                showText(text);
            //else
            //MessageBox.Show("Unknown error", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);  //if cant connect
            backbtn.Enabled = true;
        }

        private string getText()
        {
            string input = "";
            Program.msg = (Protocol.GETSITES);
            Program.waitHandle.Set();

            byte[] data = new byte[1024];
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 5554);
            UdpClient newsock = new UdpClient(ipep);
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

            data = newsock.Receive(ref sender); //"syn"
            string welcome = "ack";
            data = Encoding.ASCII.GetBytes(welcome);
            newsock.Send(data, data.Length, sender);

            Program.ewh.WaitOne();
            Program.ewh.Reset();
            input = Program.input;
            if (input.Equals(Protocol.GETSITES_ERR))
            {
                MessageBox.Show("Unknown Error.", "An Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error);  //if cant connect
                return null;
            }


            try
            {
                byte[] buffer = new byte[BUF_LEN]; // Buffer for echo string
                int recvMsgSize; // Size of received message

                // Block until receive message from a client
                do
                {
                    buffer = newsock.Receive(ref sender);
                    recvMsgSize = buffer.Length;
                } while (recvMsgSize > sizeof(int));
                int total_pack = (buffer)[0];

                byte[] longbuf = new byte[PACK_SIZE * total_pack];
                for (int i = 0; i < total_pack; i++)
                {
                    buffer = newsock.Receive(ref sender);
                    recvMsgSize = buffer.Length;
                    if (recvMsgSize != PACK_SIZE)
                    {
                        //cerr << "Received unexpected size pack:" << recvMsgSize << endl;
                        continue;
                    }
                    for (int j = 0; j < recvMsgSize; j++)
                    {
                        longbuf[(i * PACK_SIZE) + j] = buffer[j];
                    }

                }
                return Encoding.ASCII.GetString(longbuf);
            }
            catch (Exception ex)
            {
                Info.Text = "err xx " + ex;
                return null;
            }
        }

        private void showText(string text)
        {
            string result = "";
            string[] textLines = text.Split(new string[] { "\n" }, StringSplitOptions.None);

            foreach (string line in textLines)
            {
                if (line.Equals(""))
                    break;
                result += decypher(line) + "\n";
            }
            Info.Text = result;


            //string result = String.Join("\n", textLines);

        }

        private string decypher(string text)
        {
            if (text.Equals(""))
                return "";
            String result = null;
            
            RijndaelManaged rijn = new RijndaelManaged();
            rijn.Mode = CipherMode.ECB;
            rijn.Padding = PaddingMode.PKCS7;
            rijn.KeySize = (key.Length * 16);
            rijn.Key = key;
            using (MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String(text)))
            {
                using (ICryptoTransform decryptor = rijn.CreateDecryptor())
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader swDecrypt = new StreamReader(csDecrypt))
                        {
                            result = swDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            rijn.Clear();

            return result;

        }
    }
}
