@echo off
setlocal EnableDelayedExpansion
set "xIP="
set "xGood="
set "xComputer="
for /f "tokens=1" %%a in ('arp -a ^|find "dynamic"') do (
	set "xIP=%%~a"
	for /f %%f in ('ping -n 1 -w 250 !xIP! ^| findstr -m /c:"Received = 1"') do (
		for /f %%d in ('powershell.exe -executionpolicy bypass -file tcp.ps1 -hostname !xIP! ^| findstr -m /c:"True"') do (	
			echo !xIP!
			goto end
		)
	)
)

echo 0

:end
endlocal

