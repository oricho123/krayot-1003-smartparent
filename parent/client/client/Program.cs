﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace client
{
    static class Program
    {
        /*
        // Defining Constant Variables for DB
        public const string SERVER = "db4free.net";
        public const string USER = "smartparentdb"; // user name by which you want to access the database ( database user name- not server)
        public const string PASSWORD = "orilaelguylevi";
        public const string DATABASE = "smartparentdb";// the database name in the database server
        public const int DBPORT = 3306;

        public static MySqlConnection conn;
        public static MySqlCommand cmd;
        */
        public static string IP;

        private static TcpClient client;
        public static NetworkStream clientStream;

        public static AutoResetEvent waitHandle;
        public static EventWaitHandle connHandle;
        public static EventWaitHandle ewh;

        public static bool connflag;
        public static bool stopthread; // stopping the "connect" thread 

        public static Thread listenThread;
        public static Thread read;

        public static string msg;
        public static string input;
        public static byte[] byteInput;
        public static DataBase db;
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        public static void Main()
        {
            Application.EnableVisualStyles();
            connflag = false;
            stopthread = false;

            connHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
            waitHandle = new AutoResetEvent(false);
            ewh = new EventWaitHandle(false, EventResetMode.ManualReset);

            listenThread = null;
            //listenThread = new Thread(new ThreadStart(connect));
            //listenThread.IsBackground = true;   //closes thread incase of quiting program
            //listenThread.Start();
            SplashForm.ShowSplashScreen();
            Thread getIP = new Thread(new ThreadStart(getIp));
            getIP.IsBackground = true;
            getIP.Start();
            db = new DataBase();
            if (getIP.IsAlive)
                getIP.Join();
            SplashForm.CloseForm();
            
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
        }

        public static void connect()
        {
            if (IP.Equals("127.0.0.1"))
            {
                string answer = Microsoft.VisualBasic.Interaction.InputBox("Please enter the IP of the child PC manually, if you don't know how to find the IP, please refer to google.\nLeave the field empty to try to search again.", "Can't find pc", "");
                SplashForm.ShowSplashScreen();
                if (answer.Equals(""))
                {
                    Thread getIP = new Thread(new ThreadStart(getIp));
                    getIP.IsBackground = true;
                    getIP.Start();
                    getIP.Join();
                }
                else
                {
                    IP = answer;
                }
                SplashForm.CloseForm();
            }
            stopthread = false;//so the thread wont stop
            SplashForm.ShowSplashScreen();
            client = new TcpClient();
            try
            {
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(IP/*"192.168.131.128"*//*"172.20.10.4"*/), 8820);
                client.Connect(serverEndPoint);
            }
            catch (Exception e)
            {
                connflag = false;
                connHandle.Set();
                connHandle.Reset();
                SplashForm.CloseForm();
                //MessageBox.Show(e.Message, "Socket Error", MessageBoxButtons.OK, MessageBoxIcon.Error);  //if cant connect
                return;
            }
            SplashForm.CloseForm();
            connflag = true;
            connHandle.Set();
            connHandle.Reset();
            clientStream = client.GetStream();

            read = new Thread(new ThreadStart(reader)); //create thread
            read.IsBackground = true;
            read.Start();

            while (true)
            {

                waitHandle.WaitOne();
                if (stopthread)
                    break;
                if (clientStream.CanWrite)
                {
                    byte[] buffer = new ASCIIEncoding().GetBytes(msg);
                    clientStream.Write(buffer, 0, buffer.Length);
                    clientStream.Flush();
                }
            }
        }

        static void reader()  //Gets messages from server
        {
            while (true)
            {
                try // getting the msg from the server and convert to string
                {
                    byte[] buffer = new byte[256];
                    int bytesRead = clientStream.Read(buffer, 0, buffer.Length);
                    byteInput= buffer;
                    input = new ASCIIEncoding().GetString(buffer);
                    char[] msg = new char[bytesRead];
                    for (int i = 0; i < bytesRead; i++)
                    {
                        msg[i] = input[i];
                    }
                    input = new string(msg);
                    ewh.Set();  //notify the other function that a massege has recieved
                }

                catch (Exception e)//when the server closed
                {
                    //MessageBox.Show(e.Message, "Socket Error / Server may have terminated", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                    clientStream.Dispose();

                    connflag = false;

                    stopthread = true;  //cant use Thread.Abort() //not recommended
                    waitHandle.Set();

                    connHandle.Set();
                    connHandle.Reset(); //if any problems will occure, need that
                    return;
                }
            }
        }
 

        public static string handleSignin(string username,string password)
        {
            //std::vector<string> vct = msg->getValues();
            if (!db.isUserAndPassMatch(username, password))
            {
                return "1021";
            }
            return "1020";
        }

        public static string handleSignup(string username, string password, string email)
        {
            if (!Validator.isPasswordValid(password)) //check if password is legal 
            {
                return "1041";
            }
            if (!Validator.isUserNameValid(username))//check if username is legal
            {
                return "1043";
            }
            if (db.isUserExists(username)) // check if the user already exist or not
            {
                return "1042";
            }

            db.addNewUser(username, password, email);
            return "1040";
        }

        static void getIp()  //Gets IP of server
        {
            try
            {
                IP = "";
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/C findIP.bat");

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();
                // Display the command output.
                if (result.Equals(""))
                {
                    MessageBox.Show("Can't try to find IP", "Error finding IP of server", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                result = result.Remove(result.IndexOf("\r\n"), "\r\n".Length);
                if (result.Equals("0"))
                    IP = "127.0.0.1";
                else
                    IP = result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error finding IP of server", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}