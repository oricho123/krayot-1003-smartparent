﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Form6 : Form
    {
        string username;
        public Form6(string name)
        {
            username = name;
            InitializeComponent();
        }


        private void addbtn_Click(object sender, EventArgs e)
        {
            string appToBlock = urltb.Text;
            if (appToBlock.CompareTo("") == 0)
            {
                infotxt.Text = "Please fill all fields";
                infotxt.ForeColor = Color.Red;
                return;
            }

            if (Program.db.isAppBlocked(username, appToBlock))
            {
                infotxt.Text = "App is already blocked";
                infotxt.ForeColor = Color.Red;
                return;
            }

            //send to other computer
            Program.msg = (Protocol.BLOCKAPP+ Helper.getPaddedNumber(appToBlock.Length, 2) + appToBlock);
            Program.waitHandle.Set();
            Program.ewh.WaitOne();
            Program.ewh.Reset();
            string input = Program.input;
            if (input.Equals(Protocol.BLOCKAPP_OK))
            {
                infotxt.Text = "App is blocked";
                infotxt.ForeColor = Color.Green;
                Program.db.addBlockedApp(username, appToBlock);
            }
            else if (input.Equals(Protocol.BLOCKAPP_ERR))
            {
                infotxt.Text = "Error blocking app...";
                infotxt.ForeColor = Color.Red;
            }
            showApps();
        }

        private void removebtn_Click(object sender, EventArgs e)
        {
            string appToBlock = urltb.Text;
            if (appToBlock.CompareTo("") == 0)
            {
                infotxt.Text = "Please fill all fields";
                infotxt.ForeColor = Color.Red;
                return;
            }

            if (!Program.db.isAppBlocked(username, appToBlock))
            {
                infotxt.Text = "App is not blocked";
                infotxt.ForeColor = Color.Red;
                return;
            }

            //send to other computer
            Program.msg = (Protocol.UNBLOCKAPP+ Helper.getPaddedNumber(appToBlock.Length, 2) + appToBlock);
            Program.waitHandle.Set();
            Program.ewh.WaitOne();
            Program.ewh.Reset();
            string input = Program.input;
            if (input.Equals(Protocol.UNBLOCKAPP_OK))
            {
                infotxt.Text = "App is unblocked";
                infotxt.ForeColor = Color.Green;
                Program.db.deleteBlockedApp(username, appToBlock);
            }
            else if (input.Equals(Protocol.UNBLOCKAPP_ERR))
            {
                infotxt.Text = "Error unblocking app...";
                infotxt.ForeColor = Color.Red;
            }
            showApps();
        }

        private void showApps()
        {
            MySqlDataAdapter dataAdapter = Program.db.getBlockedApps(username);
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            dgv.DataSource = dataSet;
            dgv.DataMember = "Table";
            dgv.Refresh();
            dataAdapter.Dispose();
        }

        private void backbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            showApps();

        }
    }
}
