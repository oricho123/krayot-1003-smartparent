﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client
{
    static class Protocol
    {
        public const string SIGN_IN = "200";
        public const string SIGN_OUT = "201";
        public const string SIGN_IN_SUCCESS = "1020";
        public const string SIGN_IN_WRONG_DETAILS = "1021";
        public const string SIGN_IN_USER_IS_ALREADY_CONNECTED = "1022";

        public const string SIGN_UP = "203";
        public const string SIGN_UP_SUCCESS = "1040";
        public const string SIGN_UP_PASS_ILLEGAL = "1041";
        public const string SIGN_UP_USERNAME_ALREADY_EXISTS = "1042";
        public const string SIGN_UP_USERNAME_IS_ILLEGAL = "1043";
        public const string SIGN_UP_OTHER = "1044";

        public const string SIGN_OUT_PC = "205";
        public const string SEND_ROOMS = "106";

        public const string SHUT_DOWN = "207";
        public const string SEND_USERS = "108";

        public const string RESTART_PC = "209";
        public const string JOIN_ROOM_SUCCESS = "1100";
        public const string JOIN_ROOM_FAILED_ROOM_IS_FULL = "1101";
        public const string JOIN_ROOM_FAILED_ROOM_DOES_NOT_EXIST_OR_OTHER_REASON = "1102";

        public const string LOCK_KEYBOAD_MOUSE = "211";
        public const string LEAVE_ROOM_SUCCESS = "1120";

        public const string UNLOCK_KEYBOAD_MOUSE = "213";
        public const string CREATE_ROOM_SUCCESS = "1140";
        public const string CREATE_ROOM_FAIL = "1141";

        public const string GET_VIDEO = "215";
        public const string GET_VIDEO_OK = "116";

        public const string OPEN_CAMERA_OK = "118";
        public const string OPEN_CAMERA_ERR = "1180";

        public const string STOP_VIDEO = "217";
        public const string STOP_VIDEO_OK = "120";

        public const string SEND_ANSWER = "219";

        public const string END_GAME = "121";

        public const string BLOCKSITE = "222";
        public const string BLOCKSITE_OK = "223";
        public const string BLOCKSITE_ERR = "124";

        public const string UNBLOCKSITE = "225";
        public const string UNBLOCKSITE_OK = "126";
        public const string UNBLOCKSITE_ERR = "128";

        public const string BLOCKAPP = "229";
        public const string BLOCKAPP_OK = "230";
        public const string BLOCKAPP_ERR = "131";

        public const string UNBLOCKAPP = "232";
        public const string UNBLOCKAPP_OK = "133";
        public const string UNBLOCKAPP_ERR = "134";

        public const string GETSITES = "235";
        public const string GETSITES_OK = "236";
        public const string GETSITES_ERR = "137";

        public const string EXIT_APP = "299";

    }
}
