﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string password = passTB.Text;
            string username = usernameTB.Text;
            string email = emailTB.Text;
            if (username.CompareTo("") == 0 || password.CompareTo("") == 0 || email.CompareTo("") == 0)
            {
                errortxt.Text = "Please fill all fields";
                return;
            }
            string input = Program.handleSignup(username, password, email);
            /*send to other computer
            Program.msg = ("203" + Helper.getPaddedNumber(username.Length, 2) + username + Helper.getPaddedNumber(password.Length, 2) + password + Helper.getPaddedNumber(email.Length, 2) + email);
            Program.waitHandle.Set();
            Program.ewh.WaitOne();
            Program.ewh.Reset();
            string input = Program.input;
            */
            if (input.Equals("1040"))
            {
                this.Close();
            }
            else if (input.Equals("1041"))
            {
                errortxt.Text = "Password Is Illegal!! - ....";
            }
            else if (input.Equals("1042"))
            {
                errortxt.Text = "Username already exists!!";
            }
            else if (input.Equals("1043"))
            {
                errortxt.Text = "Username Is Illegal!!- ....";
            }
            else
            {
                errortxt.Text = "An error occured";
            }
        }

        private void exitbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Program.connflag == true)
            {
                Program.msg = ("201");
                Program.waitHandle.Set();
            }
            this.Close();
        }

        private void passTB_TextChanged(object sender, EventArgs e)
        {
            passTB.PasswordChar = '•';
        }

        private void Register_Load(object sender, EventArgs e)
        {

        }
    }
}
