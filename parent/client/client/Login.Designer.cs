﻿namespace client
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.usernameTB = new System.Windows.Forms.TextBox();
            this.passTB = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.singinbtn = new System.Windows.Forms.Button();
            this.errortxt = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.userPic = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.passPic = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passPic)).BeginInit();
            this.SuspendLayout();
            // 
            // usernameTB
            // 
            this.usernameTB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(39)))));
            this.usernameTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.usernameTB.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameTB.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.usernameTB.Location = new System.Drawing.Point(90, 84);
            this.usernameTB.Name = "usernameTB";
            this.usernameTB.Size = new System.Drawing.Size(137, 19);
            this.usernameTB.TabIndex = 1;
            this.usernameTB.TextChanged += new System.EventHandler(this.usernameTB_TextChanged);
            // 
            // passTB
            // 
            this.passTB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(39)))));
            this.passTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passTB.Location = new System.Drawing.Point(90, 125);
            this.passTB.Name = "passTB";
            this.passTB.Size = new System.Drawing.Size(137, 13);
            this.passTB.TabIndex = 4;
            this.passTB.TextChanged += new System.EventHandler(this.passTB_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(39)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(57, 218);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(217, 39);
            this.button1.TabIndex = 5;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // singinbtn
            // 
            this.singinbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.singinbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.singinbtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.singinbtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(39)))));
            this.singinbtn.Location = new System.Drawing.Point(57, 164);
            this.singinbtn.Name = "singinbtn";
            this.singinbtn.Size = new System.Drawing.Size(217, 39);
            this.singinbtn.TabIndex = 6;
            this.singinbtn.Text = "Sign In";
            this.singinbtn.UseVisualStyleBackColor = false;
            this.singinbtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // errortxt
            // 
            this.errortxt.AutoSize = true;
            this.errortxt.BackColor = System.Drawing.Color.Transparent;
            this.errortxt.ForeColor = System.Drawing.Color.Red;
            this.errortxt.Location = new System.Drawing.Point(121, 148);
            this.errortxt.Name = "errortxt";
            this.errortxt.Size = new System.Drawing.Size(0, 13);
            this.errortxt.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(39)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(124, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // userPic
            // 
            this.userPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userPic.BackgroundImage")));
            this.userPic.Image = ((System.Drawing.Image)(resources.GetObject("userPic.Image")));
            this.userPic.Location = new System.Drawing.Point(47, 79);
            this.userPic.Name = "userPic";
            this.userPic.Size = new System.Drawing.Size(24, 24);
            this.userPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.userPic.TabIndex = 9;
            this.userPic.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Location = new System.Drawing.Point(24, 106);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 1);
            this.panel1.TabIndex = 10;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Location = new System.Drawing.Point(24, 144);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 1);
            this.panel2.TabIndex = 11;
            // 
            // passPic
            // 
            this.passPic.Image = ((System.Drawing.Image)(resources.GetObject("passPic.Image")));
            this.passPic.Location = new System.Drawing.Point(47, 116);
            this.passPic.Name = "passPic";
            this.passPic.Size = new System.Drawing.Size(24, 24);
            this.passPic.TabIndex = 12;
            this.passPic.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(308, 303);
            this.Controls.Add(this.passPic);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.userPic);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.errortxt);
            this.Controls.Add(this.singinbtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.passTB);
            this.Controls.Add(this.usernameTB);
            this.Name = "Login";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox usernameTB;
        private System.Windows.Forms.TextBox passTB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button singinbtn;
        private System.Windows.Forms.Label errortxt;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox userPic;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox passPic;
    }
}

