﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client
{
    class Validator
    {
        public static bool isPasswordValid(string password)
        {
            if (password.Length < 4)  ///at lesat 4 notes
            {
                return false;
            }
            if (password.Contains(" ")) ///without spaces
            {
                return false;
            }
            if (!password.Any(char.IsDigit)) ///at least one digit
            {
                return false;
            }
            if (!password.Any(char.IsUpper))  ///at least one upper letter
            {
                return false;
            }
            if (!password.Any(char.IsLower))  ///at least one lower letter
            {
                return false;
            }
            return true;
        }

        public static bool isUserNameValid(string userName)
        {
            if (userName.Length < 1) ///at least 1 note
            {
                return false;
            }
            if (!char.IsLetter(userName.First())) ///first not emust be a letter
            {
                return false;
            }
            if (userName.Contains(" ")) ///no spaces
            {
                return false;
            }
            return true;
        }
    }
}
