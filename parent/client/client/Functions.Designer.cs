﻿namespace client
{
    partial class Functions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Functions));
            this.offbtn = new System.Windows.Forms.Button();
            this.restartbtn = new System.Windows.Forms.Button();
            this.signoutpcbtn = new System.Windows.Forms.Button();
            this.blockbtn = new System.Windows.Forms.Button();
            this.unblockbtn = new System.Windows.Forms.Button();
            this.blocksitebtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.signoutbtn = new System.Windows.Forms.Button();
            this.connflagtxt = new System.Windows.Forms.Label();
            this.connectbtn = new System.Windows.Forms.Button();
            this.camera = new System.Windows.Forms.Button();
            this.blockappbtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btngetLog = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // offbtn
            // 
            this.offbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.offbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.offbtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.offbtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.offbtn.Location = new System.Drawing.Point(54, 134);
            this.offbtn.Name = "offbtn";
            this.offbtn.Size = new System.Drawing.Size(127, 35);
            this.offbtn.TabIndex = 1;
            this.offbtn.Text = "Shut down";
            this.offbtn.UseVisualStyleBackColor = false;
            this.offbtn.Click += new System.EventHandler(this.offbtn_Click);
            // 
            // restartbtn
            // 
            this.restartbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.restartbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.restartbtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.restartbtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.restartbtn.Location = new System.Drawing.Point(54, 175);
            this.restartbtn.Name = "restartbtn";
            this.restartbtn.Size = new System.Drawing.Size(127, 35);
            this.restartbtn.TabIndex = 2;
            this.restartbtn.Text = "Restart";
            this.restartbtn.UseVisualStyleBackColor = false;
            this.restartbtn.Click += new System.EventHandler(this.restartbtn_Click);
            // 
            // signoutpcbtn
            // 
            this.signoutpcbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.signoutpcbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.signoutpcbtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.signoutpcbtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.signoutpcbtn.Location = new System.Drawing.Point(54, 215);
            this.signoutpcbtn.Name = "signoutpcbtn";
            this.signoutpcbtn.Size = new System.Drawing.Size(127, 35);
            this.signoutpcbtn.TabIndex = 3;
            this.signoutpcbtn.Text = "Sign out PC";
            this.signoutpcbtn.UseVisualStyleBackColor = false;
            this.signoutpcbtn.Click += new System.EventHandler(this.signoutpcbtn_Click);
            // 
            // blockbtn
            // 
            this.blockbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.blockbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.blockbtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.blockbtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.blockbtn.Location = new System.Drawing.Point(196, 134);
            this.blockbtn.Name = "blockbtn";
            this.blockbtn.Size = new System.Drawing.Size(127, 35);
            this.blockbtn.TabIndex = 4;
            this.blockbtn.Text = "Block input";
            this.blockbtn.UseVisualStyleBackColor = false;
            this.blockbtn.Click += new System.EventHandler(this.blockbtn_Click);
            // 
            // unblockbtn
            // 
            this.unblockbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.unblockbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.unblockbtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.unblockbtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.unblockbtn.Location = new System.Drawing.Point(196, 174);
            this.unblockbtn.Name = "unblockbtn";
            this.unblockbtn.Size = new System.Drawing.Size(127, 35);
            this.unblockbtn.TabIndex = 5;
            this.unblockbtn.Text = "Unblock input";
            this.unblockbtn.UseVisualStyleBackColor = false;
            this.unblockbtn.Click += new System.EventHandler(this.unblockbtn_Click);
            // 
            // blocksitebtn
            // 
            this.blocksitebtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.blocksitebtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.blocksitebtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.blocksitebtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.blocksitebtn.Location = new System.Drawing.Point(196, 215);
            this.blocksitebtn.Name = "blocksitebtn";
            this.blocksitebtn.Size = new System.Drawing.Size(127, 35);
            this.blocksitebtn.TabIndex = 6;
            this.blocksitebtn.Text = "Block Site";
            this.blocksitebtn.UseVisualStyleBackColor = false;
            this.blocksitebtn.Click += new System.EventHandler(this.blocksitebtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label2.Location = new System.Drawing.Point(14, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Hello";
            // 
            // signoutbtn
            // 
            this.signoutbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.signoutbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.signoutbtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.signoutbtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.signoutbtn.Location = new System.Drawing.Point(147, 364);
            this.signoutbtn.Name = "signoutbtn";
            this.signoutbtn.Size = new System.Drawing.Size(76, 26);
            this.signoutbtn.TabIndex = 8;
            this.signoutbtn.Text = "Sign out";
            this.signoutbtn.UseVisualStyleBackColor = false;
            this.signoutbtn.Click += new System.EventHandler(this.signoutbtn_Click);
            // 
            // connflagtxt
            // 
            this.connflagtxt.AutoSize = true;
            this.connflagtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connflagtxt.Location = new System.Drawing.Point(12, 59);
            this.connflagtxt.Name = "connflagtxt";
            this.connflagtxt.Size = new System.Drawing.Size(42, 16);
            this.connflagtxt.TabIndex = 9;
            this.connflagtxt.Text = "state";
            // 
            // connectbtn
            // 
            this.connectbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.connectbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connectbtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.connectbtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.connectbtn.Location = new System.Drawing.Point(122, 88);
            this.connectbtn.Name = "connectbtn";
            this.connectbtn.Size = new System.Drawing.Size(122, 28);
            this.connectbtn.TabIndex = 10;
            this.connectbtn.Text = "Try to connect";
            this.connectbtn.UseVisualStyleBackColor = false;
            this.connectbtn.Click += new System.EventHandler(this.connectbtn_Click);
            // 
            // camera
            // 
            this.camera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.camera.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.camera.Font = new System.Drawing.Font("Arial", 9.75F);
            this.camera.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.camera.Location = new System.Drawing.Point(54, 256);
            this.camera.Name = "camera";
            this.camera.Size = new System.Drawing.Size(127, 35);
            this.camera.TabIndex = 11;
            this.camera.Text = "Camera";
            this.camera.UseVisualStyleBackColor = false;
            this.camera.Click += new System.EventHandler(this.camera_Click);
            // 
            // blockappbtn
            // 
            this.blockappbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.blockappbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.blockappbtn.Font = new System.Drawing.Font("Arial", 9.75F);
            this.blockappbtn.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.blockappbtn.Location = new System.Drawing.Point(196, 256);
            this.blockappbtn.Name = "blockappbtn";
            this.blockappbtn.Size = new System.Drawing.Size(127, 35);
            this.blockappbtn.TabIndex = 12;
            this.blockappbtn.Text = "Block app";
            this.blockappbtn.UseVisualStyleBackColor = false;
            this.blockappbtn.Click += new System.EventHandler(this.blockappbtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(39)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(259, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // btngetLog
            // 
            this.btngetLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.btngetLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btngetLog.Font = new System.Drawing.Font("Arial", 9.75F);
            this.btngetLog.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btngetLog.Location = new System.Drawing.Point(122, 307);
            this.btngetLog.Name = "btngetLog";
            this.btngetLog.Size = new System.Drawing.Size(127, 35);
            this.btngetLog.TabIndex = 14;
            this.btngetLog.Text = "Get Activity";
            this.btngetLog.UseVisualStyleBackColor = false;
            this.btngetLog.Click += new System.EventHandler(this.btngetLog_Click);
            // 
            // Functions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(371, 402);
            this.Controls.Add(this.btngetLog);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.blockappbtn);
            this.Controls.Add(this.camera);
            this.Controls.Add(this.connectbtn);
            this.Controls.Add(this.connflagtxt);
            this.Controls.Add(this.signoutbtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.blocksitebtn);
            this.Controls.Add(this.unblockbtn);
            this.Controls.Add(this.blockbtn);
            this.Controls.Add(this.signoutpcbtn);
            this.Controls.Add(this.restartbtn);
            this.Controls.Add(this.offbtn);
            this.Name = "Functions";
            this.Text = "Functions";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form3_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button offbtn;
        private System.Windows.Forms.Button restartbtn;
        private System.Windows.Forms.Button signoutpcbtn;
        private System.Windows.Forms.Button blockbtn;
        private System.Windows.Forms.Button unblockbtn;
        private System.Windows.Forms.Button blocksitebtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button signoutbtn;
        private System.Windows.Forms.Label connflagtxt;
        private System.Windows.Forms.Button connectbtn;
        private System.Windows.Forms.Button camera;
        private System.Windows.Forms.Button blockappbtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btngetLog;
    }
}