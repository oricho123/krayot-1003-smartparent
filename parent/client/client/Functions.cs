﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace client
{

    public partial class Functions : Form
    {
        string username;
        string password;
        Thread connect;
        public Functions(string username, string password)
        {
            connect = new Thread(new ThreadStart(tryconnect)); //check if connection changed and try to connect
            connect.IsBackground = true;
           // if(connect.IsAlive ==false)
            connect.Start();
            this.password = password;
            this.username = username;

            InitializeComponent();

            label2.Text = "Hello " + username;
            /*
            if (Program.connflag == true)
            {
                connflagtxt.Text = "Connected";
                connflagtxt.ForeColor = Color.Green;
            }
            else
            {
                connflagtxt.Text = "Disconnected";
                connflagtxt.ForeColor = Color.Red;
            }*/
            connflagtxt.Text = "Disconnected";
            connflagtxt.ForeColor = Color.Red;
            lockButtons();
        }


        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Program.connflag == true)
            {
                Program.msg = ("201");
                Program.waitHandle.Set();

                //disconnect from other pc
                Program.clientStream.Dispose();

                Program.connflag = false;

                Program.stopthread = true;  //cant use Thread.Abort() //not recommended
                Program.waitHandle.Set();
            }
            connect.Abort(); // i am not using any shared resource so its fine
            this.Close();
        }

        private void signoutbtn_Click(object sender, EventArgs e)
        {
            if (Program.connflag == true)
            {
                Program.msg = ("201");
                Program.waitHandle.Set();

                //disconnect from other pc
                Program.clientStream.Dispose();

                Program.connflag = false;

                Program.stopthread = true;  //cant use Thread.Abort() //not recommended
                Program.waitHandle.Set();
            }
            connect.Abort();
            this.Close();
        }

        private void signoutpcbtn_Click(object sender, EventArgs e)
        {
            Program.msg = ("205");
            Program.waitHandle.Set();
        }

        private void offbtn_Click(object sender, EventArgs e)
        {
            Program.msg = ("207");
            Program.waitHandle.Set();
        }

        private void restartbtn_Click(object sender, EventArgs e)
        {
            Program.msg = ("209");
            Program.waitHandle.Set();
        }

        private void connectbtn_Click(object sender, EventArgs e)
        {
            Program.connHandle.Set();
            Program.connHandle.Reset(); //if any problems will occure, need that
        }

        private void tryconnect()
        {
            while (true)
            {
                Program.connHandle.WaitOne();
                Program.connHandle.Reset();
                this.Invoke((MethodInvoker)delegate {
                    connectbtn.Enabled = false;
                });
                if (Program.connflag == true)
                {
                    MessageBox.Show("You are already connected!", "Can't complete action", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    /*this.Invoke((MethodInvoker)delegate {
                        connflagtxt.Text = "Connected";
                        connflagtxt.ForeColor = Color.Green;
                    });*/
                }
                else
                {
                    if (!this.IsHandleCreated)//had an error with the invoke
                        this.CreateHandle();  //or createControl
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        connflagtxt.Text = "Disconnected";
                        connflagtxt.ForeColor = Color.Red;
                        lockButtons();
                    });
                   

                    if (Program.listenThread == null || Program.listenThread.IsAlive == false)
                    {
                        Program.listenThread = new Thread(new ThreadStart(Program.connect));
                        Program.listenThread.IsBackground = true;   //closes thread incase of quiting program
                        Program.listenThread.Start();
                    }
                    Program.connHandle.WaitOne();
                    Program.connHandle.Reset();
                    if (Program.connflag == false)
                    {
                        MessageBox.Show("The desired PC is probably turned off", "Can't Connect", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (!this.IsHandleCreated)  //had an error with the invoke
                            this.CreateControl();
                        this.Invoke((MethodInvoker)delegate()
                        {
                            connflagtxt.Text = "Connected";
                            connflagtxt.ForeColor = Color.Green;
                            unlockButtons();
                        });
                      
                        MessageBox.Show("Now you can use the funcitons :)", "Connection Success!", MessageBoxButtons.OK); //change it to a more elegant one

                        //sign in to the other computer again // dont need to check the answer, already checked in form 1
                        Program.msg = ("200" + Helper.getPaddedNumber(username.Length, 2) + username + Helper.getPaddedNumber(password.Length, 2) + password);
                        Program.waitHandle.Set();
                        Program.ewh.WaitOne();
                        Program.ewh.Reset();
                    }
                }
                this.Invoke((MethodInvoker)delegate {
                    connectbtn.Enabled = true;
                });
            }
        }

        private void camera_Click(object sender, EventArgs e)
        {
           
            this.Hide();
            Camera form4 = new Camera();

            form4.ShowDialog();

            this.Show();
        }

        private void blockbtn_Click(object sender, EventArgs e)
        {
            Program.msg = ("211");
            Program.waitHandle.Set();
        }

        private void unblockbtn_Click(object sender, EventArgs e)
        {
            Program.msg = ("213");
            Program.waitHandle.Set();
        }

        private void blocksitebtn_Click(object sender, EventArgs e)
        {
            Form5 form5 = new Form5(username);
            form5.ShowDialog();
        
        }

        private void lockButtons()
        {
            signoutpcbtn.Enabled = false;
            offbtn.Enabled = false;
            restartbtn.Enabled = false;
            camera.Enabled = false;
            blockbtn.Enabled = false;
            unblockbtn.Enabled = false;
            blocksitebtn.Enabled = false;
            blockappbtn.Enabled = false;
            btngetLog.Enabled = false;
        }

        private void unlockButtons()
        {
            signoutpcbtn.Enabled = true;
            offbtn.Enabled = true;
            restartbtn.Enabled = true;
            camera.Enabled = true;
            blockbtn.Enabled = true;
            unblockbtn.Enabled = true;
            blocksitebtn.Enabled = true;
            blockappbtn.Enabled = true;
            btngetLog.Enabled = true;
        }


        private void blockappbtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form6 form6 = new Form6(username);

            form6.ShowDialog();

            this.Show();
        }

        private void btngetLog_Click(object sender, EventArgs e)
        {
            this.Hide();
            Get_Activity activity = new Get_Activity();

            activity.ShowDialog();

            this.Show();
        }
    }
}
