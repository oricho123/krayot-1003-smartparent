﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu;
using Emgu.CV;
using Emgu.CV.Structure;

namespace client
{
    public partial class Camera : Form
    {
        private const int FRAME_HEIGHT = 720;
        private const int FRAME_WIDTH = 1280;
        private const int FRAME_INTERVAL = (1000 / 30);
        private const int PACK_SIZE = 4096; //udp pack size;
        private const int ENCODE_QUALITY = 80;
        private const int BUF_LEN = 65540; //max udp pack size;

        bool isPlaying;
        bool Pause;
        int rows, cols;

        public Camera()
        {
            InitializeComponent();
            Pause = false;
            isPlaying = false;
            rows = 0;
            cols = 0;
        }

        public async void play()
        {
            pictureBox1.Image = Properties.Resources.load;

            Program.msg = ("215");
            Program.waitHandle.Set();
           
            string input = "";
            byte[] data = new byte[1024];
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 5555);
            UdpClient newsock = new UdpClient(ipep);
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

            data = newsock.Receive(ref sender); //"syn"
            string welcome = "ack";
            data = Encoding.ASCII.GetBytes(welcome);
            newsock.Send(data, data.Length, sender);

            Program.ewh.WaitOne();
            Program.ewh.Reset();
            input = Program.input;
            if (input.Equals("1180"))
            {
                MessageBox.Show("The other computer doesnt have a camera or an error occured openeing it.", "Cannot open the web cam", MessageBoxButtons.OK, MessageBoxIcon.Error);  //if cant connect
                return;
            }
            else //if (input.StartsWith("118"))
            {
               
                string temp;
                Queue<char> q = new Queue<char>();

                for (int i = 3; i < input.Length; i++)
                {
                    q.Enqueue(input[i]);
                }

                temp = "";
                for (int i = 0; i < 4; i++)
                {
                    temp += q.Dequeue();
                }
                rows = int.Parse(temp);

                temp = "";
                for (int i = 0; i < 4; i++)
                {
                    temp += q.Dequeue();
                }
                cols = int.Parse(temp);
            }

            if((cols/rows) == (16/9)) //aspect 16:9
            {
                pictureBox1.Width = 800;
                pictureBox1.Height = 450;

            }
            else  //probably 4:3  // if not - not a problem
            {
                pictureBox1.Width = 640;
                pictureBox1.Height = 480;
            }


            try
            {
                byte[] buffer = new byte[BUF_LEN];
                int recvMsgSize; // Size of received message
                isPlaying = true;
                Pause = false;
                long last_cycle = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                while (!Pause)
                {
                    // Block until receive message from a client
                    do
                    {
                        buffer = newsock.Receive(ref sender);
                        recvMsgSize = buffer.Length;
                    } while (recvMsgSize > sizeof(int));
                    int total_pack = (buffer)[0];

                    byte[] longbuf = new byte[PACK_SIZE * total_pack];
                    for (int i = 0; i < total_pack; i++)
                    {
                        buffer = newsock.Receive(ref sender);
                        recvMsgSize = buffer.Length;
                        if (recvMsgSize != PACK_SIZE)
                        {
                            //cerr << "Received unexpected size pack:" << recvMsgSize << endl;
                            continue;
                        }
                        for (int j = 0; j < recvMsgSize; j++)
                        {
                            longbuf[(i * PACK_SIZE) + j] = buffer[j];
                        }

                    }
                    Mat mat = new Mat();
                    CvInvoke.Imdecode(longbuf, Emgu.CV.CvEnum.ImreadModes.Color, mat);
                    if(mat.Bitmap != null) 
                        pictureBox1.Image = mat.Bitmap;
                    await Task.Delay(1); //to be able to see

                    // if want to calculate fps
                    long next_cycle = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                    double duration = (next_cycle - last_cycle) / 100.0;
                    last_cycle = next_cycle;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                isPlaying = false;
                newsock.Close();
                newsock.Dispose();
                this.Close();
            }
            newsock.Close();
            newsock.Dispose();
            isPlaying = false;

        }

        private void startbtn_Click(object sender, EventArgs e)
        {
            if(!isPlaying)
                play();
        }

        

        private void Form4_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isPlaying)
            {
                Pause = true;
                System.Threading.Thread.Sleep(300);//?
                Program.msg = ("217");
                Program.waitHandle.Set();
            }
        }

        private void stopbtn_Click(object sender, EventArgs e)
        {
            if (isPlaying)
            {
                Pause = true;
                pictureBox1.Image = null;
                System.Threading.Thread.Sleep(300);//?
                Program.msg = ("217");
                Program.waitHandle.Set();
            }
            this.Close();
            // Program.ewh.WaitOne();
           // Program.ewh.Reset();
           // string input = Program.input;
           //  if(input.Equals("120"))

        }
    }
}
