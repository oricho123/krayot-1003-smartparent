﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client
{
    class Helper
    {
        public static string getPaddedNumber(int num, int digits)
        {
            return num.ToString().PadLeft(digits, '0');
        }
    }
}
