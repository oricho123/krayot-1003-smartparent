﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Form5 : Form
    {
        string username;
        public Form5(string name)
        {
            username = name;
            InitializeComponent();
        }

        private void backbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            string url = urltb.Text;
            if (url.CompareTo("") == 0)
            {
                infotxt.Text = "Please fill all fields";
                infotxt.ForeColor = Color.Red;
                return;
            }
            if(Program.db.isSiteBlocked(username, url))
            {
                infotxt.Text = "Site is already blocked";
                infotxt.ForeColor = Color.Red;
                return;
            }
            //send to other computer
            Program.msg = ("222" + Helper.getPaddedNumber(url.Length, 2) + url);
            Program.waitHandle.Set();
            Program.ewh.WaitOne();
            Program.ewh.Reset();
            string input = Program.input;
            if (input.Equals("223"))
            {
                infotxt.Text = "Site is blocked";
                infotxt.ForeColor = Color.Green;
                Program.db.addBlockedSite(username, url);
            }
            else if (input.Equals("124"))
            {
                infotxt.Text = "Error blocking site...";
                infotxt.ForeColor = Color.Red;
            }
            showSites();
        }


        private void Form5_Load(object sender, EventArgs e)
        {
            showSites();
        }

        private void removebtn_Click(object sender, EventArgs e)
        {
            string url = urltb.Text;
            if (url.CompareTo("") == 0)
            {
                infotxt.Text = "Please fill all fields";
                infotxt.ForeColor = Color.Red;
                return;
            }
            if (!Program.db.isSiteBlocked(username, url))
            {
                infotxt.Text = "Site is not blocked";
                infotxt.ForeColor = Color.Red;
                return;
            }
            //send to other computer
            Program.msg = ("225" + Helper.getPaddedNumber(url.Length, 2) + url);
            Program.waitHandle.Set();
            Program.ewh.WaitOne();
            Program.ewh.Reset();
            string input = Program.input;
            if (input.Equals("126"))
            {
                infotxt.Text = "Site is Unblocked";
                infotxt.ForeColor = Color.Green;
                Program.db.deleteBlockedSite(username, url);
            }
            else if (input.Equals("128"))
            {
                infotxt.Text = "Error unblocking site...";
                infotxt.ForeColor = Color.Red;
            }
            showSites();
        }

        private void showSites()
        {
            MySqlDataAdapter dataAdapter = Program.db.getBlockedSites(username);
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            dgv.DataSource = dataSet;
            dgv.DataMember = "Table";
            dgv.Refresh();
            dataAdapter.Dispose();
        }
    }
}
