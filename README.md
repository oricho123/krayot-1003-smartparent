Smart Parents README.md  
Written by: Guy Levy and Ori Lael

# About the project:
The project is for parents whose children are not always listening to them about going to sleep and hours limit of computer activity.
We wanted to build this project to make it easier for parents to prevent children from hiding things from their parents and help parents restrict their children.
We decided on this project because such situations happen all the time in the daily life of parents of young children, especially in our generation, and we see such a great need.

## Functions: 
 User registration and login, shutdown of the connected computer, restart the computer, sign out the user from Windows, view live video from the controlled computer's camera,
 Automatically start the software when the computer is turned on, block specific sites, lock specific applications and view the sites visited by the child.
 
## requirments to run the server:
 Windows 10  
 Python 3.6 or newer
 
## Installation:
After you will pull from the git repository you will have two folders- child and parent.
The parent computer has to open the Client executable, on the child computer, open the Server executable(preferred as administrator).

### How to run the Child file
 1) Open the **"Server"** folder  
 2) Open the **"x64"** folder  
 3) Open the **"Debug"** folder  
 4) Run the **"Server.exe"** file as administrator
 
### How to run the Parent file
 1) Open the **"client"** folder  
 2) Open the **"bin"** folder
 3) Open the **"Debug"** folder
 4) Run the **"client.exe"**
 
 
 
After you run the child, run the parent, register and login,
try to connect automatically (will work only if both computers' network sharing is public),
if faild, enter child's computer's IP manually

The application will conected between the two coputers.  
Enjoy the project and be a good spy ♥☺♥.



### Developer Instructions
 1) **Pull** the git repository, you will now have to folders.
 2) Install **"Visual Studio 2017"** or newer version.
 3) Install **"MySQL Server"**.
 4) Install **"OpenCV 3.4.2"** at "C:\".
 5) Install **"Python 3.6.6"** **64-bit** at "C:\" and add it to your PATH.
 6) Click on  **"client.sln"** to run the parents program
 7)	Click on  **"server.sln"** to run the parents program
 7.1) If you want to see the console window while developing add a comment on line 10 in DataBase.cpp
 8) If any error occurse loading the file- In the project's settings, try to check if the:
	"Additional include folders"/ "Additional library directories"/ "Additional Dependecies" Paths are correct and exist. 
