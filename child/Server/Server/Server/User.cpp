#include "User.h"

User::User(std::string name, SOCKET sck)
{
	this->_username = name;
	this->_sock = sck;
	/*this->_currRoom = nullptr;
	this->_currGame = nullptr;*/
}

void User::send(std::string message)
{
	Helper::sendData(this->_sock, message);
}

std::string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}
/*
Room * User::getRoom()
{
	return this->_currRoom;
}

Game * User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game* game)
{
	this->_currGame = game;
	this->_currRoom = nullptr;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (this->_currRoom != nullptr)
	{
		//send error message 1141
		this->send(CREATE_ROOM_FAIL);
		return false;
	}
	this->_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
	//send success message 1140
	this->send(CREATE_ROOM_SUCCESS);
	return true;
}

bool User::joinRoom(Room *newRoom)
{
	if (this->_currRoom != nullptr)
		return false;
	if (newRoom->joinRoom(this))
	{
		this->_currRoom = newRoom;
		return true;
	}
	return false;
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	int id;
	if (this->_currRoom == nullptr)
	{
		return -1;
	}
	
	id = this->_currRoom->closeRoom(this);
	if (id != -1)
	{
		this->_currRoom = nullptr;
		return id;
	}
	return -1;
}

bool User::leaveGame()
{
	bool running = false;
	if (this->_currGame != nullptr)
	{
		running =  this->_currGame->leaveGame(this);
		this->_currGame = nullptr;
	}
	return running;
}

void User::clearGame()
{
	this->_currGame = nullptr;
}
*/