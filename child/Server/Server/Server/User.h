#pragma once
/*#include "Room.h"
#include "Game.h"*/
#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include <unordered_map>
#include <exception>
#include <map>

#include "Protocol.h"
#include "Helper.h"
class Game;
class Room;

class User
{
public:
	User(std::string,SOCKET);
	void send(std::string);
	std::string getUsername();
	SOCKET getSocket();
	/*Room* getRoom();
	Game* getGame();
	void setGame(Game*);
	void clearRoom();
	bool createRoom(int, std::string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearGame();*/

	
private:
	
	std::string _username;
	SOCKET _sock;
	/*Room* _currRoom;
	Game* _currGame;*/

};
