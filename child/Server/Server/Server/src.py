#!/usr/bin/env python
# -*- coding: utf8 -*-

""" execute demo py with root privilege, and finish double dns query as follow. $ nslookup search.yahoo.com $ nslookup github.com dns sniffer will parse dns requests and responses automatically. root:scapy/ # python scapy-dns_sniff.py WARNING: No route found for IPv6 destination :: (no default route?) [*] request: 192.168.1.108:49771 -> 192.168.1.1:53 : search.yahoo.com. [*] response: 192.168.1.108:49771 <- 192.168.1.1:53 : search.yahoo.com. - ds-global.l7.search.ystg1.b.yahoo.com. [*] response: 192.168.1.108:49771 <- 192.168.1.1:53 : ds-global.l7.search.ystg1.b.yahoo.com. - ds-any-global.l7.search.ysta1.b.yahoo.com. [*] response: 192.168.1.108:49771 <- 192.168.1.1:53 : ds-any-global.l7.search.ysta1.b.yahoo.com. - 188.125.66.104 [*] request: 192.168.1.108:40813 -> 192.168.1.1:53 : github.com. [*] response: 192.168.1.108:40813 <- 192.168.1.1:53 : github.com. - 192.30.252.128 ---------------------------------------------- DNS Posioning similar to the tool called dnsspoof root:scapy/ # python scapy-dns_poisoning.py WARNING: No route found for IPv6 destination :: (no default route?) [*] request: 192.168.1.107:53052 -> 192.168.1.108:53 : search.yahoo.com. [*] response: 192.168.1.107:53052 <- 192.168.1.108:53 : search.yahoo.com. - 192.168.1.107 [*] request: 192.168.1.107:55815 -> 192.168.1.108:53 : www.google.com. [*] response: 192.168.1.107:55815 <- 192.168.1.108:53 : www.google.com. - 192.168.1.108 [*] request: 192.168.1.107:37993 -> 192.168.1.108:53 : www.microsoft.com. [*] response: 192.168.1.107:37993 <- 192.168.1.108:53 : www.microsoft.com. - 192.168.1.109 """
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR) #TO DISABLE WARININGS
from scapy.all import *
import sys
import os
import _thread
import time
from datetime import datetime
from datetime import timedelta
from threading import Timer,Lock
from Crypto.Cipher import AES
import base64

# disable verbose mode
conf.verb = 0
sites_file =open("sites.txt",'a+')
sites_file.close()
lock = Lock()
# redirect domain to the special ip
posion_table = {'www.haboreret.co.il': '127.0.0.1',
                'www.google.com': '127.0.0.1',
                'www.microsoft.com': '127.0.0.1'}
def pkt_callback(pkt):
    pkt.show()
def dns_posion(pkt):
    """posion dns request, search.yahoo.com and www.google.com will be 192.168.1.108 """
    """ parse dns request / response packet """
    if pkt and pkt.haslayer('UDP') and pkt.haslayer('DNS'):
        ip = pkt['IP']
        udp = pkt['UDP']
        dns = pkt['DNS']

        # dns query packet
        if int(udp.dport) == 53:
            qname = dns.qd.qname
            domain = qname[:-1]

            print ("\n[*] request: %s:%d -> %s:%d : %s") % (
                ip.src, udp.sport, ip.dst, udp.dport, qname)

            # match posion domain (demo, maybe not explicit)
            if domain.lower() in (posion_table.keys()):

                posion_ip = posion_table[domain]

                # send a response packet to (dns request src host)
                pkt_ip = IP(src=ip.dst,
                            dst=ip.src)

                pkt_udp = UDP(sport=udp.dport, dport=udp.sport)

                # if id is 0 (default value) ;; Warning: ID mismatch
                pkt_dns = DNS(id=dns.id,
                              qr=1,
                              qd=dns.qd,
                              an=DNSRR(rrname=qname, rdata=posion_ip))

                print ("[*] response: %s:%s <- %s:%d : %s - %s") % (
                    pkt_ip.dst, pkt_udp.dport,
                    pkt_ip.src, pkt_udp.sport,
                    pkt_dns['DNS'].an.rrname,
                    pkt_dns['DNS'].an.rdata)

                send(pkt_ip/pkt_udp/pkt_dns)


def dns_sniff(pkt):
    global lock
    """ parse dns request / response packet """
    if pkt and pkt.haslayer('UDP') and pkt.haslayer('DNS'):
        ip = pkt['IP']
        udp = pkt['UDP']
        dns = pkt['DNS']
        #if(dns.qd == ''):
          #  return
        # dns query packet
        if int(udp.dport) == 53:
            try:
                qname = dns.qd.qname
            except AttributeError:
                return
            global sites_file
            path = 'sites.txt'
            with lock:
                sites_file = open(path,'a+')

                now = datetime.now()
                title = now.strftime("%Y-%m-%d  %H:%M:%S"),' :   ',qname.decode("utf-8")
                title = ''.join(title)
                sites_file.write( ''.join((encrypt(b'orilaelguylevy12',title),'\n')) )
                #print(title)
                sites_file.close()




            #print ("\n[*] request: ",ip.src,":",udp.sport,"-> ",ip.dst,":",udp.dport," :" ,qname)

        # dns reply packet
        elif int(udp.sport) == 53:
            # dns DNSRR count (answer count)
            for i in range(dns.ancount):
                dnsrr = dns.an[i]

                #print ("\n[*] response: ",ip.dst,":",udp.dport," <-",ip.src,":", udp.sport," :" ,dnsrr.rrname," - ",dnsrr.rdata)

# Define a function for the thread
def checkStop():
   global sites_file
   global lock
   while 1:
        time.sleep(5)
        if os.path.exists("stop"):
            #print ("stopping")
            #if not sites_file.closed:
             #   sites_file.close()
            with lock:
                os.remove("stop")
                _thread.interrupt_main()#os._exit(0)#_thread.interrupt_main()#sys.exit(0) #or quit()


def getSecs():
    now=datetime.today()
    #nextDay = now.replace(hour=12, minute=0, second=0, microsecond=0)
    nextDay = now + timedelta(seconds=10)
    delta_t = nextDay - now
    secs = int(delta_t.total_seconds())
    return secs

def deleteLines():
    global t
    global sites_file
    global lock
    path = 'sites.txt'
    #if not sites_file.closed:
    with lock:
        sites_file = open(path,"r")
        lines = sites_file.readlines()
        sites_file.close()
        sites_file = open(path,"w")
        for line in lines:
            try:
                line = decrypt(b'orilaelguylevy12',line)
                year = line[0:4]
                month = line[5:7]
                day = line[8:10]
                x=datetime(int(year), int(month), int(day))
                y=datetime.today()
                if((y-x).days <8):
                    sites_file.write( ''.join((encrypt( b'orilaelguylevy12',line),'\n')) )
            except ValueError:
                pass
            except IOError:
                pass
            except IndexError:
                pass
            except UnicodeDecodeError:
                pass
            except binascii.Error:
                pass
        sites_file.close()

    t = Timer(getSecs(), deleteLines)
    t.start()

def encrypt(key,msg_text):
    msg_text = msg_text + (16 - len(msg_text) % 16) * chr(16 - len(msg_text) % 16) #pad
    msg_text=msg_text.encode("utf8")

    cipher = AES.new(key,AES.MODE_ECB)
    encoded = base64.b64encode(cipher.encrypt(msg_text)).decode("utf8")
    return(encoded)

def decrypt(key,msg_text):
    cipher = AES.new(key,AES.MODE_ECB) # never use ECB in strong systems obviously

    decoded = cipher.decrypt(base64.b64decode(msg_text)).decode("utf8")
    decoded =  decoded[:-ord(decoded[len(decoded)-1:])]  #unpad
    return(decoded)

def main():
    global t
    t = Timer(getSecs(), deleteLines)
    t.start()
    deleteLines()
    try:
        _thread.start_new_thread( checkStop,())
    except:
        print ("Error: unable to start thread")
        sys.exit()

    # capture dns request and response
    sniff(filter="udp port 53", prn=dns_sniff)

    # dns poisin (redirect domain to a special ip)
   # sniff(filter="udp port 53", prn=dns_posion)


main()
