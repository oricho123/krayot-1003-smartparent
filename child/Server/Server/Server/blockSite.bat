@echo off
taskkill /F /T /IM Chrome.exe > nul
timeout 1 > nul
pushd %appdata%
cd..
if exist "%cd%\Local\Google\Chrome\User Data\Profile 1" (
	pushd %cd%\Local\Google\Chrome\User Data\Profile 1\
	DEL Cookies-journal Cookies "Current Session" > nul
) else if exist "%cd%\Local\Google\Chrome\User Data\Default" (
	pushd %cd%\Local\Google\Chrome\User Data\Default
	DEL Cookies-journal Cookies Current Session > nul
)
popd
popd