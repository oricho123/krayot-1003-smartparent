#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS 

#include "Helper.h"

#include "RecievedMessage.h"
#include "Protocol.h"
#include "Validator.h"
#include "WSAInitializer.h"
#include "RecievedMessage.h"
#include "DataBase.h"
#include <thread>
#include <deque>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>
#include <WinSock2.h>
#include <Windows.h>

#include<opencv2/core/core.hpp>
#include<opencv2/opencv.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include "opencv2/highgui/highgui.hpp"

#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif

#define _WIN32_DCOM
#import "KillProcess.tlb" no_namespace

using namespace cv;
using namespace std;

#define PORT 8820
#define FRAME_HEIGHT 720
#define FRAME_WIDTH 1280
#define FRAME_INTERVAL (1000/30)
#define PACK_SIZE 4096 //udp pack size; note that OSX limits < 8100 bytes
#define ENCODE_QUALITY 80

class MainServer
{
public:

	MainServer();
	~MainServer();
	void serve();

	static string getCurrentDateTime(string);
	static void TRACE(const char *, ...);

private:
	void bindAndListen();
	void acceptC();   //had to change name to acceptC because of function overload
	void clientHandler(SOCKET client_socket, struct sockaddr_in client);
	void safeDeleteUser(RecievedMessage*);

	User* handleSignin(RecievedMessage*);
	bool handleSignup(RecievedMessage*);
	void handleSignout(RecievedMessage*);

	void handleSignOutFromWindows(RecievedMessage*);
	void handleShutDown(RecievedMessage*);
	void handleRestart(RecievedMessage*);
	void handleBlockInput(RecievedMessage*);
	void handleUnlockInput(RecievedMessage*);

	void handleRecievedMessages();
	void addRecievedMessages(RecievedMessage*);
	void getVideo(RecievedMessage*);
	void getSites(RecievedMessage*);
	std::vector<char> readFile(const char*);
	bool isFileExist(const std::string& name);
	void blockSite(RecievedMessage*);
	void unblockSite(RecievedMessage*);
	void killprocess();
	void writeSites();
	void stopWriteSites();
	void initBlockSites(std::string username);
	void initBlockApps(std::string username);
	void blockApp(RecievedMessage*);
	void unblockApp(RecievedMessage*);
	RecievedMessage* buildRecieveMessage(SOCKET, int, struct sockaddr_in client);

	User* getUserByName(std::string);
	User* getUserBySocket(SOCKET);

	std::thread blockAppsThread;
	std::thread writeSitesThread;
	SOCKET _socket;
	std::map<SOCKET,User*> _connectedUsers;
	DataBase _db;
	std::queue<RecievedMessage*> _queRecvMessage; //messageHandler
	std::mutex _mtxRecievedMessages;
	int roomIdSequence;
	long* stopAppBlock;
	std::condition_variable _msgQueueCondition; //CV
	bool stopvideo;

////////////////////////////////////////////
//	SOCKET _serverSocket;
//	
//	std::map<SOCKET, User*> _connectedUsers;
//	std::mutex locker;
//	std::vector<std::string> userNames;
//	std::vector<SOCKET> userSockets;
//	Helper helper;
//	void accept();
//	void clientHandler(SOCKET clientS);
///////////////////////////////////////////
};
/*
#ifndef TRACE

	#define TRACE(msg, ...)													\
	system("mkdir \"logs\">nul 2>&1");/*to create folder and block output*//*	\
	FILE* file;														\
	string path = "logs\\log_"+getCurrentDateTime("Date")+".log";	\
	fopen_s(&file,path.c_str(), "a+");								\
	if(file !=NULL){												\
	string line = getCurrentDateTime("Time") + " :\t" + msg + "\n"; \
	fprintf(file, line.c_str(), __VA_ARGS__);						\
	fclose(file);\
	delete(file)}


#endif // !TRACE
*/