#include "Validator.h"


bool Validator::isPasswordValid(string password)
{
	int i = 0;
	bool havedigits = false;

	if (password.length() < 4)
	{
		return false;
	}

	if (password.find_first_of(" ") != string::npos)
	{
		return false;
	}

	while ((i < password.length() && (!havedigits)))
	{
		if (isdigit(password[i]))
		{
			havedigits = true;
		}
		i++;
	}

	if (!havedigits)
	{
		return false;
	}

	i = 0;
	havedigits = false;
	while ((i < password.length() && (!havedigits)))
	{
		if(isupper(password[i]))
		{
			havedigits = true;
		}
		i++;
	}

	if (!havedigits)
	{
		return false;
	}

	i = 0;
	havedigits = false;
	while ((i < password.length() && (!havedigits)))
	{
		if (islower(password[i]))
		{
			havedigits = true;
		}
		i++;
	}

	if (!havedigits)
	{
		return false;
	}

	return true;
}

bool Validator::isUserNameValid(string userName)
{
	if (userName.length() < 1)
	{
		return false;
	}
	if (!isalpha(userName[0]))
	{
		return false;
	}

	if (userName.find_first_of(" ") != string::npos)
	{
		return false;
	}

	return true;
}
