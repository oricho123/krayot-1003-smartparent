#include "DataBase.h"

//ctor
DataBase::DataBase()
{
	///////////////// when the computer is restart, open again
	LONG regopenresult;
	HKEY hkey;
	char path[MAX_PATH];
	ShowWindow(GetConsoleWindow(), SW_HIDE);//hiding the console from the user
	RegOpenKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce", 0, KEY_ALL_ACCESS, &hkey); //run at startup
	RegSetValueEx(hkey, "OriServer", 0, REG_SZ, (BYTE*)path, strlen(path)); //add data
	//cout<< RegDeleteValue(hkey, "OriServer")<<endl; //to delete key
	RegCloseKey(hkey);
	////////////////////////
	bool t;
	connect = mysql_init(&mysql);

	connect = mysql_real_connect(&mysql, SERVER, USERNAME, PASSWORD, DATABASE, DBPORT, NULL, 0);  //Initialize the object

	if (connect) // check if the connection was successful, 
	{
		cout << "Connection to DB Succeeded\n";
	}
	else
	{
		cout << "Connection to DB Failed\n";
		throw std::runtime_error("Couldn't connect to the DB, probably internet problem.");
		return;
	}

	string s = "CREATE TABLE t_users(username varchar(255) not null, password text not null, email text not null,   PRIMARY KEY (username))";/// Insert a record in instructor table
	t = mysql_query(connect, s.c_str());  //creating a table; will not work if table already exist;

}

//dtor
DataBase::~DataBase()
{
	mysql_close(connect);
}

bool DataBase::isUserExists(string username)
{
	string s = "select username from t_users where username = \"" + username + "\"";
	run_show(s.c_str());
	if (result.length() != 0 && !result.compare(username))
		return true;
	return false;
}

bool DataBase::addNewUser(std::string username, std::string password, std::string email)
{
	string s = "insert into t_users values(\"" + username + "\",\"" + password + "\",\"" + email + "\")";
	bool t = mysql_query(connect, s.c_str());  // Inserting a row into instructor table, Returns Zero for success. Nonzero if an error occurred.

	if (t != 0) // check if insertion was successful, if the record is already in the table- the above query will fail because of primary key constraints
	{
		cout << " Insertion Failed: " << mysql_error(&mysql) << endl;
		return false;
	}
	return true;
	
}

bool DataBase::isUserAndPassMatch(std::string username, std::string password)
{
	string s = "select password from t_users where username =  \"" + username + "\"";
	run_show(s.c_str());
	if (result.length() != 0 && !result.compare(password))
		return true;
	return false;
}

std::vector<std::string> DataBase::getApps(std::string username)
{
	std::vector<std::string> apps;
	MYSQL_RES *res_set;
	MYSQL_ROW row;

	// Replace MySQL query with your query
	string query = "select app from t_apps where username_users = \"" + username + "\"";
	int t = mysql_query(connect, query.c_str());
	if (t != 0)
	{
		//MainServer::TRACE("quary - %s  failed",query.c_str());
		return apps;
	}
	unsigned int i = 0;

	res_set = mysql_store_result(connect);

	unsigned int numrows = mysql_num_rows(res_set);

	while (((row = mysql_fetch_row(res_set)) != NULL))
	{
		apps.push_back( row[i]);
		//cout << row[i] << endl;
	}
	return apps;
}

std::vector<std::string> DataBase::getSites(std::string username)
{
	std::vector<std::string> sites;
	MYSQL_RES *res_set;
	MYSQL_ROW row;

	// Replace MySQL query with your query
	string query = "select site from t_sites where username_users = \"" + username + "\"";
	int t = mysql_query(connect, query.c_str());
	if (t != 0)
	{
		//MainServer::TRACE("quary - %s  failed",query.c_str());
		return sites;
	}
	unsigned int i = 0;

	res_set = mysql_store_result(connect);

	unsigned int numrows = mysql_num_rows(res_set);

	while (((row = mysql_fetch_row(res_set)) != NULL))
	{
		string site = "127.0.0.1\t";
		site += row[i];
		sites.push_back(site);
		//cout << row[i] << endl;
	}
	return sites;
}


void DataBase::run_show(const char* query)
{
	bool t;
	MYSQL_RES *res_set; //Result set object to store output table from the query
	MYSQL_ROW row; // row variable to process each row from the result set.
	result = "";  //initialize for safety 

	t = mysql_query(connect, query);  // execute the query, returns Zero for success. Nonzero if an error occurred. details at https://dev.mysql.com/doc/refman/5.7/en/mysql-query.html
	if (t != 0)
	{
		cout << "quary - " << query << " failed" << endl;
		return;
	}
	res_set = mysql_store_result(connect); //reads the entire result of a query, allocates a MYSQL_RES structure, details at: https://dev.mysql.com/doc/refman/5.7/en/mysql-store-result.html
	int numrows = mysql_num_rows(res_set); // get number of rows in output table/ result set
	int num_col = mysql_num_fields(res_set); // get number of columns
	int i = 0;

	while (((row = mysql_fetch_row(res_set)) != NULL)) // fetch each row one by one from the result set
	{
		i = 0;
		while (i < num_col) { // print every row
			
			result = row[i];
			i++;
		}
		cout << endl; // print a new line after printing a row
	}
	mysql_free_result(res_set);
}


//< headline  , vector of whats inside  >
//unordered_map<string, vector<string>> results;
/*

DataBase::DataBase()
{
	int rc;
	char *zErrMsg = 0;

	// connection to the database
	rc = sqlite3_open("trivia.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		throw(std::exception("CantOpenDB"));
		system("Pause");
	}

	//Creating the tables
	// if they are already exist - nothing will happen
	rc = sqlite3_exec(db, "CREATE TABLE t_questions(question_id integer primary key autoincrement not null, question text not null, correct_ans text not null, ans2 text not null, ans3 text not null, ans4 text not null)", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "CREATE TABLE t_users(username text primarykey not null, password text not null, email text not null)", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "CREATE TABLE t_games(game_id integer primary key autoincrement not null, status integer not null, start_time DATETIME not null, end_time DATETIME)", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "CREATE TABLE t_players_answers(game_id integer not null, username text not null, question_id integer not null, player_answer text not null, is_correct integer not null, answer_time integer not null,primary key(game_id, username, question_id),foreign key(game_id) REFERENCES t_games(game_id),foreign key(username) REFERENCES t_users(username),foreign key(question_id) REFERENCES t_questions(question_id))", NULL, 0, &zErrMsg);

}

//dtor
DataBase::~DataBase()
{
	sqlite3_close(db);
}

bool DataBase::isUserExists(std::string username)
{
	clearTable();
	char *zErrMsg = 0;
	//if returns answer - user exist 
	//else does not exist
	int rc = sqlite3_exec(db, ("select username from t_users where username = \"" + username + "\"").c_str(), callback, 0, &zErrMsg);
	if (results.size() != 0 && results.begin()->second.size() != 0 && !results.begin()->second.at(0).compare(username))
		return true;
	return false;
}

bool DataBase::addNewUser(std::string username, std::string password, std::string email)
{
	int rc;
	char *zErrMsg = 0;
	rc = sqlite3_exec(db, ("insert into t_users(username, password, email) values(\"" + username + "\", \"" + password + "\", \"" + email + "\")").c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;

}

bool DataBase::isUserAndPassMatch(std::string username, std::string password)
{
	clearTable();
	char *zErrMsg = 0;
	//checking if the password matches
	int rc = sqlite3_exec(db, ("select password from t_users where username = \"" + username + "\"").c_str(), callback, 0, &zErrMsg);
	if (results.size() != 0 && results.begin()->second.size() != 0 && !results.begin()->second.at(0).compare(password))
		return true;
	return false;
}
/*

//returning wanted number of random questions
std::vector<Question*> DataBase::initQuestions(int questionsNo)
{
	std::vector<Question*> vcr;
	Question *q;
	int i;
	char *zErrMsg = 0;
	std::vector<int> ids;
	string question, currectAnswer, answer2, answer3, answer4;

	clearTable();
	sqlite3_exec(db, "select count(question_id) from t_questions", callback, 0, &zErrMsg);
	int sumQuestions = std::atoi(results.begin()->second.at(0).c_str()); //how much questions total

	for (i = 0; i < sumQuestions; i++)
	{
		ids.push_back(i + 1);   //1,2,3,4...n
	}
	std::random_shuffle(ids.begin(), ids.end());  //randomize ID's
	for (i = 0; i < questionsNo; i++)
	{
		clearTable();
		sqlite3_exec(db, ("select * from t_questions where question_id = " + std::to_string(ids.at(i))).c_str(), callback, 0, &zErrMsg);

		//taking the results and create a question
		question = results.at("question").at(0);
		currectAnswer = results.at("correct_ans").at(0);
		answer2 = results.at("ans2").at(0);
		answer3 = results.at("ans3").at(0);
		answer4 = results.at("ans4").at(0);
		q = new Question(ids.at(i), question, currectAnswer, answer2, answer3, answer4);
		vcr.push_back(q);
	}
	return vcr;
}
*/

/*
//creating string vector with 3 usernames and each one of them has score
// the 3 users are the users with the best scores
//name,score,name,score,name,score
//if there are less than 3,example name,score,0,0,0,0
std::vector<std::string> DataBase::getBestScores()
{
	std::vector<std::string> vcr;
	std::multimap<int, std::string> res;  //can have multi keys

	int sumUsers, correctAnswers;
	char *zErrMsg = 0;

	clearTable();
	sqlite3_exec(db, "select username, is_correct from t_players_answers order by username asc;", callback, 0, &zErrMsg);

	if (results.size() == 0)
		sumUsers = 0;
	else
		sumUsers = results.at("username").size();
	string username;
	int isCorrect;
	int i = 0;
	while (i < sumUsers)
	{
		username = results.at("username").at(i);
		isCorrect = 0;
		while (i < sumUsers && !results.at("username").at(i).compare(username)) //while same user
		{
			isCorrect += atoi(results.at("is_correct").at(i).c_str());
			i++;
		}
		res.insert(std::pair<int, std::string>(isCorrect, username));
	}

	std::multimap<int, std::string>::iterator it = res.end();
	if (sumUsers >= 3)
	{
		for (i = 0; i < 3; i++)
		{
			it--;
			vcr.push_back((*it).second);
			vcr.push_back(std::to_string((*it).first));
		}
	}
	else
	{
		for (i = 0; i < sumUsers; i++)
		{
			it--;
			vcr.push_back((*it).second);
			vcr.push_back(std::to_string((*it).first));
		}
		for (i = 0; i < 3 - sumUsers; i++)
		{
			vcr.push_back("");
			vcr.push_back("0");
		}
	}
	return vcr;  //name,score,name,score,name,score
}


//returning personal status of given user
//number of games,correct answers,wrong answers,avarage answer time
std::vector<std::string> DataBase::getPersonalStatus(std::string username)
{
	std::vector<std::string> vcr;
	int i, numOfGames = 0, correctAnswers = 0, wrongAnswers = 0;
	double answerTime = 0;
	char *zErrMsg = 0;
	clearTable();
	sqlite3_exec(db, ("select count() from (select * from t_players_answers where username = \"" + username + "\" group by game_id)").c_str(), callback, 0, &zErrMsg);
	numOfGames = std::atoi(results.begin()->second.at(0).c_str()); //count how much games
	if (numOfGames != 0) // if habe games
	{
		clearTable();
		sqlite3_exec(db, ("select count(is_correct) from t_players_answers where username = \"" + username + "\" group by is_correct").c_str(), callback, 0, &zErrMsg);
		if (results.size() != 0)
		{
			wrongAnswers = std::atoi(results.begin()->second.at(0).c_str()); 
			correctAnswers = std::atoi(results.begin()->second.at(1).c_str());
		}
		clearTable();
		sqlite3_exec(db, ("select avg(answer_time) from t_players_answers where username = \"" + username + "\"").c_str(), callback, 0, &zErrMsg);
		if (results.size() != 0)
		{
			answerTime = std::atof(results.begin()->second.at(0).c_str()) * 100;  //avg time in milliseconds -- 3.24 sc == 324
		}
	}

	vcr.push_back(std::to_string(numOfGames));
	vcr.push_back(std::to_string(correctAnswers));
	vcr.push_back(std::to_string(wrongAnswers));
	vcr.push_back(std::to_string((int)answerTime));
	return vcr;
}

int DataBase::insertNewGame()
{
	char *zErrMsg = 0;
	sqlite3_exec(db, "insert into t_games(status,start_time) values(0,datetime('now'))", NULL, 0, &zErrMsg);
	clearTable();
	sqlite3_exec(db, "select count(game_id) from t_games", callback, 0, &zErrMsg);
	return std::atoi(results.begin()->second.at(0).c_str());  //returns game id
}
*//*
bool DataBase::updateGameStatus(int id)
{
	/*** problem in time, three hours early////////////////////
	//////to fix : replace with - datetime('now', '3 hours');////
	char *zErrMsg = 0;
	int rc = sqlite3_exec(db, ("update t_games set status = 1 where game_id = " + std::to_string(id)).c_str(), NULL, 0, &zErrMsg);  // game is on
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	rc = sqlite3_exec(db, "update t_games set end_time = datetime('now')", NULL, 0, &zErrMsg);  // start time
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}


//adding answer to given player
//saves the answer
bool DataBase::addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime)
{
	std::string sgameid = std::to_string(gameId);
	std::string squestionid = std::to_string(questionId);
	std::string siscorrect = isCorrect ? "1" : "0";
	std::string sanswertime = std::to_string(answerTime);
	char *zErrMsg = 0;
	//start of line
	int rc = sqlite3_exec(db, ("insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) values(" +
		sgameid + ",\"" + username + "\"," + squestionid + ",\"" + answer +
		"\"," + siscorrect + "," + sanswertime + ")").c_str(), NULL, 0, &zErrMsg);
	//end of line
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

//geting resutls from the sqlite3_exec and putting them in the results
int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;
	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}
	return 0;
}

//clearing the results
void DataBase::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}*/

