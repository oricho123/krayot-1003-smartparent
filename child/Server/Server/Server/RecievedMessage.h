#pragma once

#include <iostream>
#include <string.h>
#include "User.h"

using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET, int);
	RecievedMessage(SOCKET, int, vector<string>, struct sockaddr_in client);
	SOCKET getSock();
	User* getUser();
	void setSock(SOCKET sock);
	void setUser(User* user);
	int getMessageCode();
	vector<string>& getValues();
	sockaddr_in getClientInfo();
private:
	SOCKET _sock;
	User* _user;
	int _massageCode;
	vector <string> _values;
	struct sockaddr_in client;
};


