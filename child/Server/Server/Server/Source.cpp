#pragma comment (lib, "ws2_32.lib")

#include <iostream>
#include <fstream>
#include "MainServer.h"
inline string getCurrentDateTime(string s) {
	time_t now = time(0);
	struct tm  tstruct;
	char  buf[80];
	localtime_s(&tstruct, &now);
	if (!s.compare("Time"))
		strftime(buf, sizeof(buf), "%X", &tstruct);
	else if (!s.compare("Date"))
		strftime(buf, sizeof(buf), "%Y-%m-%d", &tstruct);
	return string(buf);
};

int main()
{
	try
	{
		WSADATA wsa_data = {};
		if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
			throw std::exception("WSAStartup Failed");

		MainServer my_server;
		MainServer::TRACE("Starting...");
		// NOTICE at the end of this block the WSA will be closed 
		//WSAInitializer wsa_init;
		
		my_server.serve();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
	return 0;
}