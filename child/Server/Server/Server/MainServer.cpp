#include "MainServer.h"
MainServer::MainServer() : _db()
{
	system("RegisterDLL.exe");
	stopAppBlock = new long();
	//check if available
	try {
		*stopAppBlock = 1;
		killprocess();
		*stopAppBlock = 0;
		blockAppsThread = thread(&MainServer::killprocess, this);
		blockAppsThread.detach();
	}
	catch (...)
	{
		TRACE("Cant access functions in killproc, Run as Admin the EXE of the Program\n");
	};
	writeSitesThread = thread(&MainServer::writeSites, this);
	writeSitesThread.detach();

	this->_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

MainServer::~MainServer()
{
	TRACE(__FUNCTION__ " closing server");
	try
	{
		delete(stopAppBlock);

		for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
		{
			delete(it->second);
		}
		_connectedUsers.clear();

		::closesocket(_socket);
		WSACleanup();

	}
	catch (...) {}
}
string MainServer::getCurrentDateTime(string s) {
	time_t now = time(0);
	struct tm  tstruct;
	char  buf[80];
	localtime_s(&tstruct, &now);
	if (!s.compare("Time"))
		strftime(buf, sizeof(buf), "%X", &tstruct);
	else if (!s.compare("Date"))
		strftime(buf, sizeof(buf), "%Y-%m-%d", &tstruct);
	return string(buf);
}

void MainServer::TRACE(const char * logMsg, ...)
{
	system("mkdir \"logs\">nul 2>&1");//to create folder and block output	
	FILE* file;
	va_list args;
	string path = "logs\\log_" + getCurrentDateTime("Date") + ".log";
	fopen_s(&file, path.c_str(), "a+");
	if (file != NULL) {
		va_start(args, logMsg);
		string line = getCurrentDateTime("Time") + " :\t" + logMsg + "\n";
		vfprintf(file, line.c_str(), args);
		fclose(file);
		va_end(args);
	}


	/**
		FILE * pFile;

		string filePath = "/logs/log_" + getCurrentDateTime("date") + ".txt";
		//pFile = fopen("myfile.txt", "w");
		fprintf(pFile, logMsg.c_str());
		//string now = getCurrentDateTime("now");
		//ofstream ofs(filePath.c_str(), std::ios_base::out | std::ios_base::app);
		//ofs << now << ':\t' << logMsg << '\n';
		fclose(pFile);
		//ofs.close();*/
}

void MainServer::serve()
{
	this->bindAndListen();
	// create new thread for handling message
	std::thread tr(&MainServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		TRACE("accepting client...");
		//ShowWindow(GetConsoleWindow(), SW_HIDE);//hiding the console from the user
		this->acceptC();
	}
}

// listen to connecting requests from clients
// accept them, and create thread for each client
void MainServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr("0.0.0.0");    //Set ip 0.0.0.0
	// again stepping out to the global namespace
	if (::bind(this->_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(this->_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening (Port = %d)...", PORT);



	/*
	//////////////////////show ipv4


	string line;
	ifstream IPFile;
	int offset;
	const char* search0 = "IPv4 Address. . . . . . . . . . . : 10.0";      // search pattern

	system("ipconfig > ip.txt");

	IPFile.open("ip.txt");
	if (IPFile.is_open())
	{
		while (!IPFile.eof())
		{
			getline(IPFile, line);
			if ((offset = line.find(search0, 0)) != string::npos)
			{
				//   IPv4 Address. . . . . . . . . . . : 1
				//1234567890123456789012345678901234567890     
				line.erase(0, 39);
				cout << line << endl;
				IPFile.close();
			}
		}
	}
	remove("ip.txt");
	////////////////
	*/ 
}

void MainServer::acceptC()  
{
	struct sockaddr_in client_info = { 0 };
	int size = sizeof(client_info);
	SOCKET client_socket = accept(_socket, (sockaddr*)&client_info, &size);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	
	TRACE("Client accepted. Client socket = %d", client_socket);
	// create new thread for client	and detach from it
	std::thread tr(&MainServer::clientHandler, this, client_socket , client_info);
	tr.detach();
}

void MainServer::clientHandler(SOCKET client_socket, struct sockaddr_in client)
{
	RecievedMessage* currRcvMsg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(client_socket);

		while (msgCode != 0 && msgCode != std::atoi(END_GAME))
		{
			currRcvMsg = this->buildRecieveMessage(client_socket, msgCode, client);
			this->addRecievedMessages(currRcvMsg);

			msgCode = Helper::getMessageTypeCode(client_socket);
		}

		currRcvMsg = this->buildRecieveMessage(client_socket, std::atoi(END_GAME), client);
		this->addRecievedMessages(currRcvMsg);

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currRcvMsg = this->buildRecieveMessage(client_socket, std::atoi(END_GAME), client);
		this->addRecievedMessages(currRcvMsg);
	}
	//closesocket(client_socket);
}

RecievedMessage * MainServer::buildRecieveMessage(SOCKET client_socket, int msgCode, struct sockaddr_in client)
{
	RecievedMessage* msg = nullptr;
	vector<string> values;
	User* user = getUserBySocket(client_socket);

	if (!std::to_string(msgCode).compare(SIGN_IN))
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		int passwordSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passwordSize);

		values.push_back(userName);
		values.push_back(password);
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(SIGN_OUT) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(SIGN_UP) == 0)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string UserName = Helper::getStringPartFromSocket(client_socket, userSize);

		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passSize);

		int emailSize = Helper::getIntPartFromSocket(client_socket, 2);
		string email = Helper::getStringPartFromSocket(client_socket, emailSize);
		values.push_back(UserName);
		values.push_back(password);
		values.push_back(email);

		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(SIGN_OUT_PC) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(SHUT_DOWN) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(RESTART_PC) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(LOCK_KEYBOAD_MOUSE) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(UNLOCK_KEYBOAD_MOUSE) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(GET_VIDEO) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(STOP_VIDEO) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(BLOCKSITE) == 0)
	{
		int siteSize = Helper::getIntPartFromSocket(client_socket, 2);
		string siteName = Helper::getStringPartFromSocket(client_socket, siteSize);

		values.push_back(siteName);
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(UNBLOCKSITE) == 0)
	{
		int siteSize = Helper::getIntPartFromSocket(client_socket, 2);
		string siteName = Helper::getStringPartFromSocket(client_socket, siteSize);

		values.push_back(siteName);
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(BLOCKAPP) == 0)
	{
		int siteSize = Helper::getIntPartFromSocket(client_socket, 2);
		string siteName = Helper::getStringPartFromSocket(client_socket, siteSize);

		values.push_back(siteName);
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(UNBLOCKAPP) == 0)
	{
		int siteSize = Helper::getIntPartFromSocket(client_socket, 2);
		string siteName = Helper::getStringPartFromSocket(client_socket, siteSize);

		values.push_back(siteName);
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(GETSITES) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(SEND_ANSWER) == 0)
	{
		string answerNo = Helper::getStringPartFromSocket(client_socket, 1);
		string answerTime = Helper::getStringPartFromSocket(client_socket, 2);

		values.push_back(answerNo);
		values.push_back(answerTime);

		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(END_GAME) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	else if (std::to_string(msgCode).compare(EXIT_APP) == 0)
	{
		msg = new RecievedMessage(client_socket, msgCode, values, client);
	}
	if (msg != nullptr)
		msg->setUser(user);
	return msg;
}

void MainServer::addRecievedMessages(RecievedMessage *msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);

	this->_queRecvMessage.push(msg);
	lck.unlock();
	this->_msgQueueCondition.notify_all();
}


void MainServer::handleRecievedMessages()
{
	string msgCode = "";
	SOCKET clientSock = 0;
	string userName;
	while (true)
	{
		/*try
		{*/
		unique_lock<mutex> lck(_mtxRecievedMessages);

		// Wait for clients to enter the queue.
		if (_queRecvMessage.empty())
			_msgQueueCondition.wait(lck);

		// in case the queue is empty.
		if (_queRecvMessage.empty())
			continue;

		RecievedMessage* currMessage = _queRecvMessage.front();
		_queRecvMessage.pop();
		lck.unlock();
//				unique_lock<mutex> lck(_mtxRecievedMessages);
//				currRcvMessage = _queRcvMessages.front();
//				_queRcvMessages.pop();
//				lck.unlock();

		try
		{
			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			currMessage->setUser(this->getUserBySocket(clientSock));
			msgCode = std::to_string(currMessage->getMessageCode());

			TRACE("--------------------------------------");
			TRACE("handleRecievedMessages: msgCode = %s, client_socket: %d", msgCode.c_str(), currMessage->getSock());

			if (msgCode.compare(SIGN_IN) == 0)
			{
				User* user = handleSignin(currMessage);
				if (user != nullptr)
					this->_connectedUsers.insert(std::pair<SOCKET, User*>(clientSock, user));
			}
			else if (msgCode.compare(SIGN_OUT) == 0)
			{
				handleSignout(currMessage);
			}
			else if (msgCode.compare(SIGN_UP) == 0)
			{
				handleSignup(currMessage);
			}
			else if (msgCode.compare(SIGN_OUT_PC) == 0)
			{
				handleSignOutFromWindows(currMessage);
			}
			else if (msgCode.compare(SHUT_DOWN) == 0)
			{
				handleShutDown(currMessage);
			}
			else if (msgCode.compare(RESTART_PC) == 0)
			{
				handleRestart(currMessage);
			}
			else if (msgCode.compare(LOCK_KEYBOAD_MOUSE) == 0)
			{
				handleBlockInput(currMessage);
			}
			else if (msgCode.compare(UNLOCK_KEYBOAD_MOUSE) == 0)
			{
				handleUnlockInput(currMessage);
			}
			else if(msgCode.compare(GET_VIDEO) == 0)
			{
				std::thread tr(&MainServer::getVideo,this, new RecievedMessage(*currMessage));  //created a new one so it wont get deleted
				tr.detach();
			}
			else if (msgCode.compare(STOP_VIDEO) == 0)
			{
				this->stopvideo = true;
			}
			else if (msgCode.compare(BLOCKSITE) == 0)
			{
				blockSite(currMessage);
			}
			else if (msgCode.compare(UNBLOCKSITE) == 0)
			{
				unblockSite(currMessage);
			}
			else if (msgCode.compare(BLOCKAPP) == 0)
			{
				blockApp(currMessage);
			}
			else if (msgCode.compare(UNBLOCKAPP) == 0)
			{
				unblockApp(currMessage);
			}
			else if (msgCode.compare(GETSITES) == 0)
			{
				getSites(currMessage);
			}
			else if (msgCode.compare(EXIT_APP) == 0)
			{
				safeDeleteUser(currMessage);
			}
			else
			{
				safeDeleteUser(currMessage);
			}

			delete currMessage;
		}
		catch (...)
		{
			this->safeDeleteUser(currMessage);
		}
		TRACE("--------------------------------------");
	}
}


void MainServer::safeDeleteUser(RecievedMessage* msg)
{
	try
	{
		SOCKET s = msg->getSock();
		this->handleSignout(msg);
		TRACE("Closing socket = %d", s);
		closesocket(s);
		TRACE("User has disconnected: socket = %d", s);
	}
	catch (...)
	{}
}

// Msg Code: 200
// if connected successfuly return user object
// if not return null
User * MainServer::handleSignin(RecievedMessage *msg)
{
	SOCKET client_socket = msg->getSock();
	// get details
	vector<string> vals = msg->getValues();
	string userName = vals[0];
	string passWord = vals[1];
	if (!this->_db.isUserAndPassMatch(userName, passWord))
	{
		Helper::sendData(msg->getSock(), SIGN_IN_WRONG_DETAILS);
		TRACE("SEND: User try sign in with wrong details: username = %s, socket = %d", userName.c_str(), client_socket);
		return nullptr;
	}
	User* currUser = this->getUserByName(userName);
	if (currUser != nullptr)
	{
		Helper::sendData(msg->getSock(), SIGN_IN_USER_IS_ALREADY_CONNECTED);
		TRACE("SEND: User already connected: username = %s, socket = %d", userName.c_str(), currUser->getSocket());
		return nullptr;
	}
	User* user = new User(userName, msg->getSock());
	TRACE("Adding new user to connected users list: socket = %d, username = %s", client_socket, userName.c_str());
	this->_connectedUsers.insert(std::pair<SOCKET, User*>(user->getSocket(), user));

	TRACE("SEND: User signed in successfuly: username = %s, socket = %d", userName.c_str(), client_socket);
	user->send(SIGN_IN_SUCCESS);
	initBlockApps(userName);
	return user;
}

// Msg Code: 203
bool MainServer::handleSignup(RecievedMessage *msg)
{
	vector<string> vals = msg->getValues();
	// get details
	SOCKET client_socket = msg->getSock();
	string userName = vals[0];
	string passWord = vals[1];
	string email = vals[2];


	if (!Validator::isPasswordValid(passWord))
	{
		Helper::sendData(msg->getSock(), SIGN_UP_PASS_ILLEGAL);
		TRACE("SEND: User signup failed. Invalid password: username = %s, socket = %d", passWord.c_str(), client_socket);
		return false;
	}
	if (!Validator::isUserNameValid(userName))
	{
		Helper::sendData(msg->getSock(), SIGN_UP_USERNAME_IS_ILLEGAL);
		TRACE("SEND: User signup failed. Invalid username: username = %s, socket = %d", userName.c_str(), client_socket);
		return false;
	}
	if (this->_db.isUserExists(userName))
	{
		Helper::sendData(msg->getSock(), SIGN_UP_USERNAME_ALREADY_EXISTS);
		TRACE("SEND: User signup failed. User already exist: username = %s, socket = %d", userName.c_str(), client_socket);
		return false;
	}
	if (this->_db.addNewUser(userName, passWord, email))
	{
		Helper::sendData(msg->getSock(), SIGN_UP_SUCCESS);
		TRACE("SEND: User signed up successfuly: username = %s, socket = %d", userName.c_str(), client_socket);
		return true;
	}
	Helper::sendData(msg->getSock(), SIGN_UP_OTHER);
	TRACE("SEND: User signup failed. Failed insert record to DB: username = %s, socket = %d", userName.c_str(), client_socket);
	return false;
}



void MainServer::handleSignout(RecievedMessage *msg)
{
	if (msg->getUser() != nullptr)
	{
		this->_connectedUsers.erase(msg->getUser()->getSocket());
		TRACE("Erase user from connected users list: username = %s, socket = %d", msg->getUser()->getUsername().c_str(), msg->getSock());
		_connectedUsers.erase(msg->getSock());
		TRACE("User had signedout: username = %s, socket = %d", msg->getUser()->getUsername().c_str(), msg->getSock());
	}
}
void MainServer::handleSignOutFromWindows(RecievedMessage *msg)
{
	TRACE("Signing user out of Windows");
	//safeDeleteUser(msg);
	try {
		system("shutdown -f");
		exit(0);
	}
	catch (exception &e)
	{
	}
}
void MainServer::handleShutDown(RecievedMessage *msg)
{
	TRACE("Shutting down PC");
	//safeDeleteUser(msg);
	try
	{
		system("shutdown -t 0 -f -s");// t - means the time, f - means force, s - means shut down
		exit(0);
	}
	catch (exception &e)
	{
	}
}
void MainServer::handleRestart(RecievedMessage *msg)
{
	TRACE("Restarting PC");
	//safeDeleteUser(msg);
	try
	{
		system("shutdown -t 0 -f -r");// t - means the time, f - means force, r - means restart
		exit(0);
	}
	catch (exception &e)
	{
	}
}

void MainServer::handleBlockInput(RecievedMessage* msg)
{
	TRACE("Blocking Input");
	BlockInput(true); //lock the keyboard and the mouse //will try to catch when is the user pressing ctrl+alt+del to encouter the bypass
}

void MainServer::handleUnlockInput(RecievedMessage* msg)
{
	TRACE("Unblocking Input");
	BlockInput(false);
}

void MainServer::getVideo(RecievedMessage *msg)
{
	TRACE("Camera - Creating UDP connection");
	int sock, length, n;
	WSAInitializer wsa_init;
	struct sockaddr_in server, from; // IP Addressing(ip, port, type)...
	char buffer[256] = { 0 };
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock== INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr(inet_ntoa(msg->getClientInfo().sin_addr));
	server.sin_port = htons(5555);
	//Helper::sendData(msg->getSock(), GET_VIDEO_OK); //?
	length = sizeof(struct sockaddr_in);
	n = sendto(sock, "syn", 4, 0, (struct sockaddr *)&server, length);
	if (n < 0)
	{
		TRACE(__FUNCTION__ " - send syn");
		Helper::sendData(msg->getSock(), OPEN_CAMERA_ERR);
		return;
	}
	n = recvfrom(sock, buffer, 256, 0, (struct sockaddr *)&from, &length); //Got an ack

	if (n < 0)
	{
		TRACE(__FUNCTION__ " - recieve ack");
		Helper::sendData(msg->getSock(), OPEN_CAMERA_ERR);
		return;
	}


	try {
		//UDPSocket sock;
		int jpegqual = ENCODE_QUALITY; // Compression Parameter

		Mat frame, send;
		vector < uchar > encoded;

		this->stopvideo = false;
		TRACE("Camera - Opening camera");
		VideoCapture cap(0);  // open the default camera 
		if (!cap.isOpened()) {
			TRACE("Camera - Cannot open the web cam");
			Helper::sendData(msg->getSock(), OPEN_CAMERA_ERR);
			return;
		}
		cap >> frame;
	
		const int rows = frame.rows;
		const int cols = frame.cols;
		string message = OPEN_CAMERA_OK;
		message.append(Helper::getPaddedNumber(rows, 4));
		message.append(Helper::getPaddedNumber(cols, 4));
		Helper::sendData(msg->getSock(), message);
		TRACE("Camera - Sending Video");
		clock_t last_cycle = clock();
		while (!this->stopvideo) {
			cap >> frame;
			if (frame.size().width == 0)
				continue;//if error occured...
			resize(frame, send, Size(FRAME_WIDTH, FRAME_HEIGHT), 0, 0, INTER_LINEAR);
			vector < int > compression_params;
			compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
			compression_params.push_back(jpegqual);

			imencode(".jpg", send, encoded, compression_params);
			int total_pack = 1 + (encoded.size() - 1) / PACK_SIZE; //number of packeges/chunks

			int ibuf[1];
			ibuf[0] = total_pack;
			n = sendto(sock, (const char*)ibuf, sizeof(int), 0, (struct sockaddr *)&server, length);

			for (int i = 0; i < total_pack; i++)
				sendto(sock, (const char*)&encoded[i * PACK_SIZE], PACK_SIZE, 0, (struct sockaddr *)&server, length);
			waitKey(FRAME_INTERVAL);

			clock_t next_cycle = clock();
			double duration = (next_cycle - last_cycle) / (double)CLOCKS_PER_SEC;
			last_cycle = next_cycle;
		}
		TRACE("Camera - Video Stopped");
		cap.release();
		//delete(cap);
		closesocket(sock);
	}
	catch (Exception & e) {
		TRACE("Camera Error - ", e.what());
	}
	delete msg;

}

void MainServer::getSites(RecievedMessage *msg)
{
	thread stop = thread(&MainServer::stopWriteSites, this);
	stop.detach();
	if (stop.joinable())
		stop.join();
	TRACE("GetSites - Creating UDP connection");
	int sock, length, n;
	WSAInitializer wsa_init;
	struct sockaddr_in server, from; // IP Addressing(ip, port, type)...
	char buffer[256] = { 0 };
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " error - socket");
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr(inet_ntoa(msg->getClientInfo().sin_addr));
	server.sin_port = htons(5554);
	//Helper::sendData(msg->getSock(), GET_VIDEO_OK); //?
	length = sizeof(struct sockaddr_in);
	n = sendto(sock, "syn", 4, 0, (struct sockaddr *)&server, length);
	if (n < 0)
	{
		TRACE(__FUNCTION__ " error - send syn");
		Helper::sendData(msg->getSock(), GETSITES_ERR);
		return;
	}
	n = recvfrom(sock, buffer, 256, 0, (struct sockaddr *)&from, &length); //Got an ack

	if (n < 0)
	{
		TRACE(__FUNCTION__ " error - recieve ack");
		Helper::sendData(msg->getSock(), GETSITES_ERR);
		return;
	}

	std::vector<char> encoded = readFile("sites.txt");


	try {
		
		Helper::sendData(msg->getSock(), GETSITES_OK);
		int total_pack = 1 + (encoded.size() - 1) / PACK_SIZE;

		int ibuf[1];
		ibuf[0] = total_pack;
		n = sendto(sock, (const char*)ibuf, sizeof(int), 0, (struct sockaddr *)&server, length);
		//sock.sendTo(ibuf, sizeof(int), (struct sockaddr *)&server, length);
		TRACE("GetSites - Sending Sites");
		for (int i = 0; i < total_pack; i++)
			sendto(sock, (const char*)&encoded[i * PACK_SIZE], PACK_SIZE, 0, (struct sockaddr *)&server, length);
		
	}
	catch (Exception & e) {
		TRACE("GetSites Error - ", e.what());
	}

	
	writeSitesThread = thread(&MainServer::writeSites, this);
	writeSitesThread.detach();
}

std::vector<char> MainServer::readFile(const char *filename)
{
	// open the file:
	std::ifstream file(filename, std::ios::in);

	// To get spaces!!!
	file.unsetf(std::ios::skipws);

	// get its size:
	std::streampos fileSize;

	file.seekg(0, std::ios::end);
	fileSize = file.tellg();
	file.seekg(0, std::ios::beg);

	// reserve capacity
	std::vector<char> vec;
	vec.reserve(fileSize);

	// read the data:
	vec.insert(vec.begin(),
		std::istream_iterator<char>(file),
		std::istream_iterator<char>());
	vec.push_back('\n\0');

	return vec;
}

bool MainServer::isFileExist(const std::string & name)
{
	FILE *file = nullptr;
	fopen_s(&file, name.c_str(), "r");
	if (file != nullptr) {
		fclose(file);
		return true;
	}
	else {
		return false;
	}
}


void MainServer::blockSite(RecievedMessage *msg)
{
	char site[1000], ch;
	ifstream in;
	ofstream out;
	string siteName = msg->getValues().at(0);
	TRACE("Blocking Site - %s",siteName.c_str());

	out.open("C:/Windows/System32/drivers/etc/hosts", ios::app);
	if (!out)
	{
		TRACE("Either File Not Found or Permission Denied, Run as Admin the EXE of the Program\n");
		Helper::sendData(msg->getSock(), BLOCKSITE_ERR);
	}
	else
	{
		system("blockSite.bat");
		out << "\n127.0.0.1" << "\t" << siteName;
		TRACE("%s - Has been blocked", siteName.c_str());
		Helper::sendData(msg->getSock(), BLOCKSITE_OK);
	}
	out.close();
}

void MainServer::unblockSite(RecievedMessage *msg)
{
	string siteName = msg->getValues().at(0);
	string line;
	TRACE("Unlocking Site - %s", siteName.c_str());

	ifstream fin;
	fin.open("C:/Windows/System32/drivers/etc/hosts");
	ofstream temp;
	temp.open("C:/Windows/System32/drivers/etc/temp");
	if (!temp || !fin)
	{
		TRACE("Either File Not Found or Permission Denied, Run as Admin the EXE of the Program\n");
		Helper::sendData(msg->getSock(), UNBLOCKSITE_ERR);
		if (temp)
			temp.close();
		if (fin)
			fin.close();
		return;
	}

	while (getline(fin, line))
	{
		if (line.find(siteName) == std::string::npos && line.compare(""))
		{
			temp << line << "\n";
		}
	}

	temp.close();
	fin.close();
	remove("C:/Windows/System32/drivers/etc/hosts");
	rename("C:/Windows/System32/drivers/etc/temp", "C:/Windows/System32/drivers/etc/hosts");
	TRACE("%s - Has been unblocked", siteName.c_str());

	Helper::sendData(msg->getSock(), UNBLOCKSITE_OK);
}

void MainServer::killprocess()
{
	//	try
	//	{
	HRESULT hr = CoInitialize(NULL);
	// Create the interface pointer.
	IClass1Ptr obj;
	obj.CreateInstance(__uuidof(Class1));
	obj->killproc(stopAppBlock);
	// Uninitialize COM.
	CoUninitialize();
	if (*stopAppBlock == 0)
		TRACE(__FUNCTION__ " - stopped!!");
	//}
//catch (...)
//{
//	std::cout << "Cant run commands " << "Try opening as administrator or reinstalling";
//	return;
//};
}

void MainServer::writeSites()
{
	TRACE("Starting Sites sniffer");
	PyObject *pName, *pModule, *pFunc;
	PyObject *pValue;
	char* funcname = (char*)"start";
	// Initialize the Python interpreter
	Py_Initialize();
	//PyRun_SimpleString("from scapy.all import *");

	// Build the name object
	pName = PyUnicode_FromString("src.py");
	// Load the module object
	pModule = PyImport_Import(pName);
	Py_DECREF(pName);
	if (pModule != NULL) {
		////
		pFunc = PyObject_GetAttrString(pModule, "main");
		if (pFunc && PyCallable_Check(pFunc)) {



			pValue = PyObject_CallObject(pFunc, NULL);
		}
		else {
			if (PyErr_Occurred())
				PyErr_Print();
			//fprintf(stderr, "Cannot find function \"%s\"\n", "main");
		}
		Py_XDECREF(pFunc);
		Py_DECREF(pModule);
		/////
	}
	else {
		//if (PyErr_Occurred())
			//PyErr_Print();
		//fprintf(stderr, "Failed to load \"%s\"\n", "src.py");
		return ;
	}
	Py_Finalize();
	return ;
}

void MainServer::stopWriteSites()
{
	TRACE("Stopping Sites sniffer");
	FILE *file;
	fopen_s(&file,"stop", "w");
	fclose(file);
	while (isFileExist("stop")) {
		//Sleep(500);
		int x = 0;
		for (size_t i = 0; i < 1000000; i++)
		{
			x++;
		}
	}
	if (writeSitesThread.joinable())
		writeSitesThread.join();
}

void MainServer::initBlockSites(std::string username)
{
	char site[1000], ch;
	ifstream in;
	ofstream out;
	//string siteName = msg->getValues().at(0);
	TRACE("Initializing Block Sites");

	out.open("C:/Windows/System32/drivers/etc/hosts", ios::trunc);
	if (!out)
	{
		TRACE("Either File Not Found or Permission Denied, Run as Admin the EXE of the Program\n");
	}
	else
	{
		system("blockSite.bat");

		std::vector<std::string> sites = _db.getSites(username);
		std::ostream_iterator<std::string> output_iterator(out, "\n");
		std::copy(sites.begin(), sites.end(), output_iterator);

		TRACE("Initializing Block Sites");
	}
	out.close();



}

void MainServer::initBlockApps(std::string username)
{
	*stopAppBlock = 1;
	ofstream out;
	//string appName = msg->getValues().at(0);
	TRACE("Initializing Block Apps");
	//wait for thread to stop
	if (blockAppsThread.joinable())
		blockAppsThread.join();
	//check if function available
	try {
		*stopAppBlock = 1;
		killprocess();
	}
	catch (...)
	{
		TRACE("Cant access functions in killproc, Run as Admin the EXE of the Program\n");
		return;
	};

	out.open("apps.txt", ios::trunc);
	if (!out)
	{
		TRACE("Error opening apps.txt\n");
		return;
	}
	std::vector<std::string> apps = _db.getApps(username);
	std::ostream_iterator<std::string> output_iterator(out, "\n");
	std::copy(apps.begin(), apps.end(), output_iterator);

	out.close();

	*stopAppBlock = 0;
	blockAppsThread = thread(&MainServer::killprocess, this);
	blockAppsThread.detach();
	TRACE("Blocked Apps Has been Initialized");

}

void MainServer::blockApp(RecievedMessage *msg)
{
	*stopAppBlock = 1;
	ofstream out;
	string appName = msg->getValues().at(0);
	TRACE("Blocking App - %s", appName.c_str());
	//wait for thread to stop
	if (blockAppsThread.joinable())
		blockAppsThread.join();
	//check if function available
	try {
		*stopAppBlock = 1;
		killprocess();
	}
	catch (...)
	{
		TRACE("Cant access functions in killproc, Run as Admin the EXE of the Program\n");
		Helper::sendData(msg->getSock(), BLOCKAPP_ERR);
		return;
	};

	out.open("apps.txt", ios::app);
	if (!out)
	{
		TRACE("Error opening apps.txt\n");
		Helper::sendData(msg->getSock(), BLOCKAPP_ERR);
		return;
	}
	out << appName << "\n";
	out.close();

	*stopAppBlock = 0;
	blockAppsThread = thread(&MainServer::killprocess, this);
	blockAppsThread.detach();
	TRACE("%s - Has been blocked", appName.c_str());

	Helper::sendData(msg->getSock(), BLOCKAPP_OK);

}

void MainServer::unblockApp(RecievedMessage *msg)
{
	*stopAppBlock = 1;
	string appName = msg->getValues().at(0);
	TRACE("Unlocking App - %s", appName.c_str());
	string line;

	ifstream fin;
	fin.open("apps.txt");
	ofstream temp;
	temp.open("temp.txt");
	if (!temp || !fin)
	{
		TRACE("Error opening apps.txt\n");
		Helper::sendData(msg->getSock(), UNBLOCKAPP_ERR);
		if (temp)
			temp.close();
		if (fin)
			fin.close();
		return;
	}

	while (getline(fin, line))
	{
		if (line.find(appName) == std::string::npos && line.compare(""))
		{
			temp << line << "\n";
		}
	}

	temp.close();
	fin.close();
	remove("apps.txt");
	rename("temp.txt", "apps.txt");
	

	//wait for thread to stop
	if (blockAppsThread.joinable())
		blockAppsThread.join();
	//check if function available
	try {
		*stopAppBlock = 1;
		killprocess();
	}
	catch (...)
	{
		TRACE("Cant access functions in killproc, Run as Admin the EXE of the Program\n");
		Helper::sendData(msg->getSock(), UNBLOCKAPP_ERR);
		return;
	};

	*stopAppBlock = 0;
	blockAppsThread = thread(&MainServer::killprocess, this);
	blockAppsThread.detach();
	TRACE("%s - Has been unblocked", appName.c_str());
	Helper::sendData(msg->getSock(), UNBLOCKAPP_OK);
}


User* MainServer::getUserByName(std::string UserName)
{
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (!(*it).second->getUsername().compare(UserName))
			return it->second;
	}
	return nullptr;
}

User * MainServer::getUserBySocket(SOCKET client_socket)
{
	auto it = this->_connectedUsers.find(client_socket);
	if (it != this->_connectedUsers.end())
		return it->second;
	return nullptr;
}

