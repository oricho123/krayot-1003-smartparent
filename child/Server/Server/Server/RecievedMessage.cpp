#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET a, int b)
{
	this->_sock = a;
	this->_massageCode = b;
	this->_user = nullptr;
}


RecievedMessage::RecievedMessage(SOCKET sock, int code, vector<string> val, struct sockaddr_in client)
{
	this->_user = nullptr;
	this->_sock = sock;
	this->_massageCode = code;
	this->_values = val;
	this->client = client;
}


SOCKET RecievedMessage::getSock()
{
	return this->_sock;
}


User* RecievedMessage::getUser()
{
	return this->_user;
}


void RecievedMessage::setSock(SOCKET sock)
{
	this->_sock = sock;
}


void RecievedMessage::setUser(User* user)
{
	this->_user = user;
}


int RecievedMessage::getMessageCode()
{
	return this->_massageCode;
}


vector<string>& RecievedMessage::getValues()
{
	return this->_values;
}

sockaddr_in RecievedMessage::getClientInfo()
{
	return this->client;
}
