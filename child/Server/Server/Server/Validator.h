#include <iostream>
#include <string.h>

using namespace std;

class Validator
{
public:
	static bool isPasswordValid(string password);
	static bool isUserNameValid(string userName);

};